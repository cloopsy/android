﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Util;

namespace CloopsyAndroid
{
	//Source: https://developer.xamarin.com/guides/android/user_interface/creating_a_splash_screen/

	[Activity(Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
	public class SplashActivity : AppCompatActivity
	{
		static readonly string TAG = "X:" + typeof(SplashActivity).Name;
		bool isLogged = false;

		public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
		{
			base.OnCreate(savedInstanceState, persistentState);
			Log.Debug(TAG, "SplashActivity.OnCreate");
		}

		protected override void OnResume()
		{
			base.OnResume();

			Task startupWork = new Task(() =>
			{
				Log.Debug(TAG, "Startup splash screen - showing logos");
				Services.Bootstrap(Application.Context);
				Task.Delay(5000); // Simulate a bit of startup work.
				Log.Debug(TAG, "Working in the background - important stuff.");
			});

			startupWork.ContinueWith(t =>
			{
				Log.Debug(TAG, "Work is finished - start Login activity.");
				RestLibrary.Settings.ISONLINE = Services.IsThereConnection();
				Console.WriteLine("Connected:" + RestLibrary.Settings.ISONLINE);

				Android.Support.V7.App.AlertDialog.Builder builder = new Android.Support.V7.App.AlertDialog.Builder(this);
				Android.Support.V7.App.AlertDialog alertDialog = builder.Create();

				if (!RestLibrary.Settings.ISONLINE && RestLibrary.Settings.LOGINKEY == string.Empty)
				{
					alertDialog.SetTitle("Login failed");
					alertDialog.SetMessage("You're offline and you have never logged in from this device");
					alertDialog.Show();
				}
				else if (!RestLibrary.Settings.ISONLINE && RestLibrary.Settings.LOGINKEY != string.Empty)
				{
					alertDialog.SetTitle("Offline login");
					alertDialog.SetMessage("You're offline but you are logged in");
					alertDialog.Show();
					isLogged = Services.Bootstrap(this);
					isLogged = true;
				}
				else
				{
					isLogged = Services.Bootstrap(this);
					if (isLogged == false)
					{
						StartActivity(typeof(LoginActivity));
					}
				}

				Services.UserService.get(
					 (res) =>
					 {
						System.Console.WriteLine("Tutorial: " + RestLibrary.Settings.TutorialDone);
						 if (RestLibrary.Settings.TutorialDone)
							StartActivity(typeof(HomeActivity));
						 else
							StartActivity(typeof(TutorialActivity));
					 },
					 (err) =>
					 {
						StartActivity(typeof(LoginActivity));
					 }
				 );							
			 }, TaskScheduler.FromCurrentSynchronizationContext());

			startupWork.Start();
		}
	}
}


