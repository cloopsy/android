﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/
using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using RestLibrary.Models;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace CloopsyAndroid
{
	public delegate void OnDeletedField();
	public class ReportListAdapter : BaseAdapter<object>
	{
		JToken reports;
		Context context;
		public OnDeletedField OnDeleted;

		public ReportListAdapter(Context context, JToken reports) : base()
		{
			this.reports = reports;
			this.context = context;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override object this[int position]
		{
			get
			{
				return reports[position];
			}
		}

		public override int Count
		{
			get { return reports.Count(); }
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var obj = reports[position];
			var txt = (obj != null ? obj["created_at"].ToString() : "");
			Console.WriteLine("Value: " + txt);

			//if (convertView == null)
			//{
			//	//var obj = reports[position];
			//	var value = (obj != null ? obj["created_at"].ToString().ToCharArray() : "".ToCharArray());

			//	convertView = new LinearLayout(context);
			//	var view = (LinearLayout)convertView;

			//	var labelView = new TextView(context);
			//	labelView.SetText(value, 0, value.Length);

			//	var cancelButton = new Button(context);
			//	cancelButton.Text = "Cancel";
			//	cancelButton.Click += delegate {
			//		Services.ReportService.delete(
			//			(int)obj["id"],
			//			delegate {
			//				OnDeleted();
			//			},
			//			delegate
			//			{
			//				OnDeleted();
			//			}
			//		);
			//	};

			//	var editButton = new Button(context);
			//	editButton.Text = "Edit";
			//	editButton.Click += delegate {
			//		var intent = new Intent(context, typeof(ReportEditActivity));
			//		intent.PutExtra("id", (int)obj["id"]);
			//		context.StartActivity(intent);
			//	};


			//view.AddView(labelView);
			//view.AddView(cancelButton);
			//view.AddView(editButton);

			//	return convertView;
			//}
			//return convertView;

			View row = convertView;
			if (row == null)
			{
				row = LayoutInflater.FromContext(context).Inflate(Resource.Layout.Report_listview_layout, null, false);
			}
			TextView txtReportDate = row.FindViewById<TextView>(Resource.Id.txtReportDate);
			Button btnEdit = row.FindViewById<Button>(Resource.Id.btnEdit);
			Button btnDelete = row.FindViewById<Button>(Resource.Id.btnDelete);

			txtReportDate.Text = txt;
			btnEdit.Click += delegate {
				var intent = new Intent(context, typeof(ReportEditActivity));
				intent.PutExtra("id", (int)obj["id"]);
				context.StartActivity(intent);
			};
			btnDelete.Click += delegate {
				Services.ReportService.delete(
						(int)obj["id"],
						delegate {
							OnDeleted();
						},
						delegate
						{
							OnDeleted();
						}
					);
			};
			return row;

		}
	}
}

