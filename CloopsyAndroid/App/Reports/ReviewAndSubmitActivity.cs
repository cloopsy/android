﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;

namespace CloopsyAndroid
{
	[Activity(Label = "Confirm Data", Icon = "@mipmap/icon", Theme = "@style/MyTheme", NoHistory = true)]
	public class ReviewAndSubmitActivity : AppCompatActivity
	{
		private Android.Support.V7.Widget.Toolbar mToolbar;
		private ImageView mImageView;
		private List<string> mSelectionDataset;
		private ListView mSelectionList;
		private ArrayAdapter mSelectionArrayAdapter;
		private Button mBtnSubmit;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Review_layout);

			mImageView = FindViewById<ImageView>(Resource.Id.imageView1);
			mToolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			mSelectionList = FindViewById<ListView>(Resource.Id.selectionListView);
			mBtnSubmit = FindViewById<Button>(Resource.Id.buttonSubmit);

			SetSupportActionBar(mToolbar);
			SupportActionBar.SetTitle(Resource.String.CloopsyCategory);

			//TODO TEST
			//int height = Resources.DisplayMetrics.HeightPixels;
			//int width = mImageView.Width;
			//ReportDetail.bitmap = ReportDetail._file.Path.LoadAndResizeBitmap(width, height);
			//ReportDetail.bitmap = BitmapHelpers.LoadAndResizeBitmap(ReportDetail._dir + "/" + ReportDetail._filename, width, height);
			if (ReportDetail.bitmap != null)
			{
				mImageView.SetImageBitmap(ReportDetail.bitmap);
				ReportDetail.bitmap.Dispose();
				ReportDetail.bitmap = null;
			}
			GC.Collect();

			mSelectionDataset = new List<string>();
			mSelectionDataset.Add(ReportDetail.latitude);
			mSelectionDataset.Add(ReportDetail.longitude);
			mSelectionDataset.Add(ReportDetail.compass ?? "NO AVAILABLE COMPASS");
			//mSelectionDataset.Add(ReportDetail.macrocategory);
			mSelectionDataset.Add(ReportDetail.category_name);
			//mSelectionDataset.Add(ReportDetail.subcategory);

			mSelectionArrayAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, mSelectionDataset);
			mSelectionList.Adapter = mSelectionArrayAdapter;

			//var path = ReportDetail._file.Path;
			var path = ReportDetail._dir + "/" + ReportDetail._filename;
			var stream = new FileStream(path, FileMode.Open);

			ReportDetail.telephone_id = Android.OS.Build.Serial;

			mBtnSubmit.Click += delegate {
				ProgressDialog progressDialog = ProgressDialog.Show(this, "", "Uploading...", true);
				progressDialog.SetProgressStyle(ProgressDialogStyle.Spinner);
				Services.ReportService.create(
					new Dictionary<string, object>()
					{
						{"category", ReportDetail.category},
						{"position", ReportDetail.longitude + ", " + ReportDetail.latitude},
						{"direction", ReportDetail.compass},
						{"distance_focus", ReportDetail.distance_focus ?? "DISTANCE FOCUS NO AVAILABLE"},
						{"telephone_id", ReportDetail.telephone_id},
						{"image", stream}
					},
					(res) =>
					{
						progressDialog.Hide();
						var intent = new Intent(this, typeof(HomeActivity));
						intent.PutExtra("submitReportResult", res.ToString());
						stream.Close();
						ReportDetail.ReportReset();
						StartActivity(typeof(HomeActivity));
					},
					(err) =>
					{
						progressDialog.Hide();
						var intent = new Intent(this, typeof(HomeActivity));
						intent.PutExtra("submitReportResult", err);
						stream.Close();
						ReportDetail.ReportReset();
						StartActivity(intent);
					}
				);
			};

		}
	}
}
