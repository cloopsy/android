﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using RestLibrary.Models;

namespace CloopsyAndroid
{
	[Activity(Label = "Edit Report", Icon = "@mipmap/icon", Theme = "@style/MyTheme", WindowSoftInputMode = SoftInput.AdjustPan)]
	public class ReportEditActivity : AppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.ReportUpdate_layout);

			ListView reportListView = FindViewById<ListView>(Resource.Id.reportListView);
			Button reportUpdateButton = FindViewById<Button>(Resource.Id.reportUpdateButton);

			Services.ReportService.get(
				Intent.GetIntExtra("id", 0),
				(res) =>
				{
					var report = res.ToObject<Report<string>>();
					reportListView.Adapter = new ReportEditAdapter(this, report);
				},
				Console.WriteLine,
				Console.WriteLine
			);

			reportUpdateButton.Click +=
				delegate
				{
					var data = ReportEditAdapter.getDataFromView(reportListView);
					Services.ReportService.update(
						data,
						(res) => { Console.WriteLine(res); },
						(err) => { Console.WriteLine(err); }
					);
				};
		}
	}
}

