﻿using Android.Content;
using Android.Views;
using Android.Widget;
using RestLibrary.Models;
using System;
using System.Collections.Generic;

namespace CloopsyAndroid
{
	public class UserAdapter : BaseAdapter<object>
	{

		User user;
		Context context;


		public UserAdapter(Context context, User user): base()
		{
			this.user = user;
			this.context = context;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override object this[int position]
		{
			get
			{
				return user.GetType().GetProperty(GetItemPropriety(position)).GetValue(user);
			}
		}

		public override int Count
		{
			get { return User.FIELDS.Length; }
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				var prop = GetItemPropriety(position);
				var obj = this[position];
				var value = (obj != null ? obj.ToString().ToCharArray() : "".ToCharArray());

				convertView = new LinearLayout(context);
				var view = (LinearLayout)convertView;

				var labelView = new TextView(context);

				if (Array.IndexOf(User.READ_ONLY_FIELD, prop) > -1)
				{
					var valueView = new TextView(context);
					view.AddView();
					valueView.SetText(value, 0, value.Length);
				}
				else
				{
					convertView = new EditText(context);
					((EditText)convertView).InputType = Android.Text.InputTypes.TextVariationShortMessage;
					((EditText)convertView).SetEms(15);
					((EditText)convertView).SetText(value, 0, value.Length);
				}
				return convertView;
			}
			return convertView;
		}

		public static Dictionary<string, object> getDataFromView(ListView view)
		{
			var output = new Dictionary<string, object>(); 
			for (int index = 0; index < view.ChildCount; ++index)
			{
				var nextChild = (TextView)view.GetChildAt(index);
				nextChild.InputType = Android.Text.InputTypes.ClassText;
				var text = nextChild.Text;
				var label = GetItemPropriety(index);
				output.Add(label, text);
			}
			return output;
		}

		public static string GetItemPropriety(int position)
		{
			return User.FIELDS[position];
		}
	}
}

