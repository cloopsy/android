﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Locations;
using Android.Net;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace CloopsyAndroid
{
	[Activity(Label = "Cloopsy Home", Icon = "@mipmap/icon", Theme = "@style/MyTheme", LaunchMode = Android.Content.PM.LaunchMode.SingleTop)]
	public class HomeActivity : AppCompatActivity, ILocationListener
	{
		private Button mBtnAdd;
		private Android.Support.V7.Widget.Toolbar mToolbar;
		private CloopsyBarDrawerToggle mDrawerToggle;
		private DrawerLayout mDrawerLayout;
		private ListView mLeftDrawer;
		private ArrayAdapter mLeftAdapter;
		private List<string> mLeftDataSet;
		private ReportListFragment reportListFragment;
		private TextView waitingGPSTextView;

		LocationManager locationManager;
		string locationProvider;
		Location currentLocation;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.Home_layout);
			mBtnAdd = FindViewById<Button>(Resource.Id.btnAdd);
			mBtnAdd.Enabled = false;
			waitingGPSTextView = FindViewById<TextView>(Resource.Id.waitingGPSTextView);
			mToolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			mLeftDrawer = FindViewById<ListView>(Resource.Id.left_drawer);

			SetSupportActionBar(mToolbar);
			SupportActionBar.SetTitle(Resource.String.CloopsyAddReport);

			mLeftDataSet = new List<string>();
			mLeftDataSet.Add("Account");
			mLeftDataSet.Add("WebGIS");
			mLeftDataSet.Add("Tutorial");
			mLeftDataSet.Add("Offline reports");
			mLeftDataSet.Add("Credits");
			mLeftDataSet.Add("Info on submitted reports");
			mLeftDataSet.Add("Logout");
			mLeftAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, mLeftDataSet);
			mLeftDrawer.Adapter = mLeftAdapter;

			var fragmentTransaction = this.FragmentManager.BeginTransaction();
			reportListFragment = new ReportListFragment();
			fragmentTransaction.Add(Resource.Id.ReportListFragmentContainer, reportListFragment);
			fragmentTransaction.Commit();

			Android.Support.V7.App.AlertDialog.Builder builder = new Android.Support.V7.App.AlertDialog.Builder(this);
			Android.Support.V7.App.AlertDialog alertDialog = builder.Create();

			mLeftDrawer.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
			{
				string opt_clicked = mLeftDataSet[e.Position];

				switch (opt_clicked)
				{
					case "Account":
						StartActivity(typeof(UserEditActivity));
						break;

					case "WebGIS":
						//var uri = Android.Net.Uri.Parse("https://cloopsy.unipv.it/");
						//var intent = new Intent(Intent.ActionView, uri);
						//StartActivity(intent);
						if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
						{
							StartActivity(typeof(WebgisActivity));
						}
						else 
						{
							var uri = Android.Net.Uri.Parse("https://cloopsy.unipv.it/dist/webgis-mobile.html?token="+RestLibrary.Settings.LOGINKEY);
							var intent = new Intent(Intent.ActionView, uri);
							StartActivity(intent);
						}
						break;

					case "Offline reports":
						System.Console.WriteLine("Offline mode requested");
						alertDialog.SetTitle("Offline reports");
						alertDialog.SetMessage("This feature will be implemented soon");
						alertDialog.Show();
						break;

					case "Tutorial":
						StartActivity(typeof(TutorialActivity));
						break;

					case "Credits":
						//alertDialog.SetTitle("Info");
						//alertDialog.SetMessage("Developer Daniele De Vecchi, Remote Sensing Group, TLC Lab, Università degli Studi di Pavia");
						//alertDialog.Show();
						StartActivity(typeof(CreditsActivity));
						//System.Console.WriteLine("Developer Daniele De Vecchi, Remote Sensing Group, TLC Lab, Università degli Studi di Pavia");
						break;

					case "Info on submitted reports":
						alertDialog.SetTitle("Info on submitted reports");
						alertDialog.SetMessage("Submitted reports will be released to public only after admin approval (in order to avoid issues). You can always check your reports from the webGIS service by selecting the 'Your Reports' layer");
						alertDialog.Show();
						break;
						
					case "Logout":
						Services.AuthService.logout(then: () => {
							StartActivity(typeof(LoginActivity));
						});
						break;
				}

			};

			mDrawerToggle = new CloopsyBarDrawerToggle(this, mDrawerLayout, Resource.String.ReportOpenDrawer, Resource.String.CloopsyAddReport);

			mDrawerLayout.SetDrawerListener(mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled(true);
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetDisplayShowTitleEnabled(true);
			mDrawerToggle.SyncState();

			mBtnAdd.Click += delegate{
				ReportDetail.updateGPSCord = true;
				ReportDetail.updateCompass = true;
				if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
				{
					StartActivity(typeof(PersonalCamera2Activity));
				}
				else
					StartActivity(typeof(PersonalCameraActivity));
			};

			if (Settings.isTest)
			{
				ReportDetail.FakeGPS();
			}
			else
			{
				System.Console.WriteLine("Initialize location manager");
				InitializeLocationManager();
			}

			ReportDetail.validGPS += delegate
			{
				RunOnUiThread(() =>
				{
					mBtnAdd.Enabled = true;
					waitingGPSTextView.Visibility = ViewStates.Invisible;
					System.Console.WriteLine(mBtnAdd.Enabled);
				});
			};
			ReportDetail.invalidGPS += delegate
			{
				RunOnUiThread(() =>
				{
					mBtnAdd.Enabled = false;
					waitingGPSTextView.Visibility = ViewStates.Visible;
					System.Console.WriteLine(mBtnAdd.Enabled);
				});
			};

		}

		protected override void OnPostResume()
		{
			base.OnPostResume();
			Android.Support.V7.App.AlertDialog.Builder builder = new Android.Support.V7.App.AlertDialog.Builder(this);
			Android.Support.V7.App.AlertDialog alertDialog = builder.Create();
			var intentSubmitReportResult = (Intent.GetStringExtra("submitReportResult") ?? "");
			if (intentSubmitReportResult != "")
			{
				alertDialog.SetTitle("Report Submit Result");
				alertDialog.SetMessage(Intent.GetStringExtra("submitReportResult"));
				alertDialog.Show();
			}
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			mDrawerToggle.OnOptionsItemSelected(item);
			return base.OnOptionsItemSelected(item);
		}

		void InitializeLocationManager()
		{
			locationManager = (LocationManager)GetSystemService(LocationService);
			Criteria locationCriteria = new Criteria { Accuracy = Accuracy.Fine };
			//locationProvider = locationManager.GetBestProvider(locationCriteria, true);
			if (locationManager.AllProviders.Contains(LocationManager.NetworkProvider) && locationManager.IsProviderEnabled(LocationManager.NetworkProvider))
			{
				locationProvider = LocationManager.NetworkProvider;
			}
			else if (locationManager.AllProviders.Contains(LocationManager.GpsProvider) && locationManager.IsProviderEnabled(LocationManager.GpsProvider))
			{
				locationProvider = LocationManager.GpsProvider;
			}

			if (locationProvider != null)
			{
				locationManager.RequestLocationUpdates(locationProvider, 0, 10, this);
				System.Console.WriteLine(locationProvider);
			}
			else
			{
				System.Console.WriteLine("No Location provider");
			}
			//IList<string> acceptableLocationProviders = locationManager.GetProviders(locationCriteria, true);
			//if (acceptableLocationProviders.Any())
			//{
			//	locationProvider = acceptableLocationProviders.First();
			//}
			//else
			//{
			//	locationProvider = string.Empty;
			//	System.Console.WriteLine("No Location provider");
			//}

		}

		public void OnLocationChanged(Location location)
		{
			currentLocation = location;
			if (currentLocation == null)
			{
				System.Console.WriteLine("Unable to determine location");
			}
			else
			{
				System.Console.WriteLine(string.Format("{0:f6},{1:f6}", currentLocation.Latitude, currentLocation.Longitude));
				mBtnAdd.Enabled = true;
				waitingGPSTextView.Visibility = ViewStates.Invisible;
				ReportDetail.latitude = currentLocation.Latitude.ToString(CultureInfo.InvariantCulture);
				//System.Console.WriteLine("Lat: " + ReportDetail.latitude);
				ReportDetail.longitude = currentLocation.Longitude.ToString(CultureInfo.InvariantCulture);
				//System.Console.WriteLine("Lon: " + ReportDetail.longitude);
			}
		}

		public void OnProviderDisabled(string provider) { }

		public void OnProviderEnabled(string provider) { }

		public void OnStatusChanged(string provider, Availability status, Bundle extras) {
			System.Console.WriteLine("Status GPS: " + status);
			ReportDetail.StatusGPS = status;
		}

	}
}

