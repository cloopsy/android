﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.OS;
using Android.Hardware;
using System.Globalization;

namespace CloopsyAndroid
{
	[Activity(Label = "Camera2Basic", Icon = "@mipmap/icon", Theme = "@style/MyTheme")]
	public class PersonalCamera2Activity : AppCompatActivity,ISensorEventListener
	{
		private SensorManager mSensorManager;
		static readonly object _syncLock = new object();

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			//ActionBar.Hide();
			SetContentView(Resource.Layout.Camera2_personal_layout);
			InitializeSensorManager();
			if (bundle == null)
			{
				FragmentManager.BeginTransaction().Replace(Resource.Id.container, Camera2BasicFragment.NewInstance()).Commit();
			}
		}

		protected override void OnResume()
		{
			base.OnResume();
			mSensorManager.RegisterListener(this, mSensorManager.GetDefaultSensor(SensorType.Orientation), SensorDelay.Fastest);
			//mSensorManager.RegisterListener(this, mSensorManager.GetDefaultSensor(SensorType.Accelerometer), SensorDelay.Game);
			//smSensorManager.RegisterListener(this, mSensorManager.GetDefaultSensor(SensorType.GeomagneticRotationVector), SensorDelay.Game);
		}

		void InitializeSensorManager()
		{
			mSensorManager = (SensorManager)GetSystemService(Context.SensorService);
		}

		void ISensorEventListener.OnAccuracyChanged(Sensor sensor, SensorStatus accuracy) { }

		void ISensorEventListener.OnSensorChanged(SensorEvent e)
		{
			lock (_syncLock)
			{
				//System.Console.WriteLine("Orientation: " + e.Values[0]);
				//ReportDetail.compass = "" + e.Values[0];
				ReportDetail.compass = e.Values[0].ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}