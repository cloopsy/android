﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Hardware;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using Android.Util;
using System.Collections;
using System.Globalization;

namespace CloopsyAndroid
{
	[Activity(Label = "Cloopsy Camera", Icon = "@mipmap/icon", Theme = "@style/MyTheme", LaunchMode = Android.Content.PM.LaunchMode.SingleTop)]
	public class PersonalCameraActivity : AppCompatActivity, Android.Hardware.Camera.IPictureCallback, Android.Hardware.Camera.IPreviewCallback, Android.Hardware.Camera.IShutterCallback,ISurfaceHolderCallback, Android.Hardware.Camera.IAutoFocusCallback,ISensorEventListener
	{
		static Android.Hardware.Camera camera = null;
		static Android.Hardware.Camera.Parameters cameraParameters;
		private SensorManager mSensorManager;
		static readonly object _syncLock = new object();
		int surfaceWidth;
		int surfaceHeight;

		private float[] gravity;
		private float[] magnetic;
		private float[] accels = new float[3];
		private float[] mags = new float[3];
		private float[] values = new float[3];
		private float azimuth;
		private float pitch;
		private float roll;


		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			ReportDetail.ReportReset();
			CreateDirectoryForPictures();
			InitializeSensorManager();

			ReportDetail._filename = string.Format("myPhoto_{0}.jpg", Guid.NewGuid());
			//ReportDetail._file = new FileOutputStream(ReportDetail._dir + "/" + ReportDetail._filename);
			System.Console.WriteLine(ReportDetail._filename);

			SetContentView(Resource.Layout.Camera_personal_layout);
			SurfaceView surface = (SurfaceView)FindViewById(Resource.Id.surfaceView);
			surfaceWidth = Resources.DisplayMetrics.WidthPixels;
			surfaceHeight = surfaceWidth * 4 / 3;
			System.Console.WriteLine(surfaceWidth);
			System.Console.WriteLine(surfaceHeight);

			var holder = surface.Holder;
			holder.AddCallback(this);
			holder.SetType(Android.Views.SurfaceType.PushBuffers);

			surface.Touch += TouchFocus;

			Button takePicture = (Button)FindViewById(Resource.Id.captureImage);
			takePicture.Click += FocusAndPicture;
			//takePicture.Click += delegate
			//	{
			//		Android.Hardware.Camera.Parameters p = camera.GetParameters();
			//		p.PictureFormat = Android.Graphics.ImageFormatType.Jpeg;
			//		camera.SetParameters(p);
			//		camera.TakePicture(this, this, this);
			//	};
			// Create your application here
		}


		private void CreateDirectoryForPictures()
		{
			ReportDetail._dir = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "Cloopsy");
			if (!ReportDetail._dir.Exists())
			{
				ReportDetail._dir.Mkdirs();
			}
		}

		void Android.Hardware.Camera.IPictureCallback.OnPictureTaken(byte[] data, Android.Hardware.Camera camera)
		{
			if (data != null)
			{
				try
				{
					//outStream = new FileOutputStream(dataDir + "/" + PICTURE_FILENAME);
					//outStream.Write(data);
					//outStream.Close();
					ReportDetail._file = new FileOutputStream(ReportDetail._dir + "/" + ReportDetail._filename);
					System.Console.WriteLine(ReportDetail._dir + "/" + ReportDetail._filename);
					ReportDetail._file.Write(data);
					ReportDetail._file.Close();
					ReportDetail.bitmap = BitmapHelpers.FixRotation(ReportDetail._dir + "/" + ReportDetail._filename);
					//ReportDetail.bitmap = BitmapFactory.DecodeFile(ReportDetail._dir + "/" + ReportDetail._filename);
					//int height = Resources.DisplayMetrics.HeightPixels;
					//int width = height * 4 / 3;
					int height = 640;
					int width = 480;
					ReportDetail.bitmap = BitmapHelpers.LoadAndResizeBitmap(ReportDetail._dir + "/" + ReportDetail._filename, width, height);
					mSensorManager.UnregisterListener(this);
					ReportDetail.updateCompass = false;
					ReportDetail.updateGPSCord = false;
					StartActivity(typeof(CategoryActivity));
				}
				catch (FileNotFoundException e)
				{
					System.Console.Out.WriteLine(e.Message);
				}
				catch (IOException ie)
				{
					System.Console.Out.WriteLine(ie.Message);
				}
			}
			//camera.Unlock();
			//camera.Unlock();
			//camera.StopPreview();
			//camera.SetPreviewCallback(null);
			//camera.Release();
			//camera = null;
		}

		void Android.Hardware.Camera.IPreviewCallback.OnPreviewFrame(byte[] data, Android.Hardware.Camera camera)
		{
			
		}

		void Android.Hardware.Camera.IShutterCallback.OnShutter()
		{
			
		}


		void ISurfaceHolderCallback.SurfaceChanged(ISurfaceHolder holder, Format format, int width, int height)
		{
			
		}

		void ISurfaceHolderCallback.SurfaceCreated(ISurfaceHolder holder)
		{
			try
			{
				if (camera != null)
				{
					camera.Release();
				}
				camera = Android.Hardware.Camera.Open();
				//Android.Hardware.Camera.Parameters p = camera.GetParameters();
				//p.PictureFormat = Android.Graphics.ImageFormatType.Jpeg;
				cameraParameters = camera.GetParameters();
				cameraParameters.PictureFormat = Android.Graphics.ImageFormatType.Jpeg;
				//cameraParameters.SetPreviewSize(surfaceWidth, surfaceHeight);
				//cameraParameters.SetRotation(90);
				camera.SetDisplayOrientation(90);
				camera.SetParameters(cameraParameters);
				camera.SetPreviewCallback(this);
				//camera.Lock();

				camera.SetPreviewDisplay(holder);
				camera.StartPreview();
			}
			catch (IOException e)
			{
			}
		}

		void ISurfaceHolderCallback.SurfaceDestroyed(ISurfaceHolder holder)
		{
			//if (camera != null)
			//{
			//	camera.Unlock();
			//	camera.StopPreview();
			//	camera.SetPreviewCallback(null);
			//	camera.Release();Android.Hardware.Camera.Parameters p = camera.GetParameters();
			//		p.PictureFormat = Android.Graphics.ImageFormatType.Jpeg;
			//		camera.SetParameters(p);
			//		camera.TakePicture(this, this, this);
			//	camera = null;
			//}
		}


		protected override void OnPause()
		{
			base.OnPause();
			if (camera != null)
			{
				camera.StopPreview();
				camera.SetPreviewCallback(null);
				camera.Lock();
				camera.Release();
				camera = null;
			}
		}

		protected override void OnResume()
		{
			base.OnResume();
			mSensorManager.RegisterListener(this, mSensorManager.GetDefaultSensor(SensorType.Orientation), SensorDelay.Fastest);

			//mSensorManager.RegisterListener(this, mSensorManager.GetDefaultSensor(SensorType.Accelerometer), SensorDelay.Game);
			//smSensorManager.RegisterListener(this, mSensorManager.GetDefaultSensor(SensorType.GeomagneticRotationVector), SensorDelay.Game);
		}

		public override void OnBackPressed()
		{
			ReportDetail.ReportReset();
			base.OnBackPressed();
		}

		void Android.Hardware.Camera.IAutoFocusCallback.OnAutoFocus(bool success, Android.Hardware.Camera camera)
		{
			
			if (success)
			{
				
			}
		}

		private void FocusAndPicture(object sender, EventArgs e)
		{
			//cameraParameters.PictureFormat = Android.Graphics.ImageFormatType.Jpeg;
			//camera.SetParameters(p);
			camera.TakePicture(this, this, this);
		}

		private void TouchFocus(object sender, View.TouchEventArgs touchEventArgs)
		{
			//if ((camera != null) && (e.Action == MotionEventActions.Down))
			//{
			//	System.Console.WriteLine("Touch event");
			//	camera.CancelAutoFocus();
			//	//Rect focusRect = calculateTapArea(e.GetX(), e.GetY(), 1f);
			//	//Rect meteringRect = calculateTapArea(e.GetX(), e.GetY(), 1.5f);
			//	float x = e.GetX();
			//	float y = e.GetY();
			//	float touchMajor = e.TouchMajor;
			//	float touchMinor = e.TouchMinor;
			//	Rect touchRect = new Rect((int)(x - touchMajor / 2), (int)(y - touchMinor / 2), (int)(x + touchMajor / 2), (int)(y + touchMinor / 2));

			//	this.submitFocusAreaRect(touchRect);
			//}

			switch (touchEventArgs.Event.Action)
			{
				case MotionEventActions.Down:
					if (camera != null)
					{
						System.Console.WriteLine("Touch event");
						camera.CancelAutoFocus();
						float x = touchEventArgs.Event.GetX();
						float y = touchEventArgs.Event.GetY();
						float touchMajor = touchEventArgs.Event.TouchMajor;
						float touchMinor = touchEventArgs.Event.TouchMinor;
						Rect touchRect = new Rect((int)(x - touchMajor / 2), (int)(y - touchMinor / 2), (int)(x + touchMajor / 2), (int)(y + touchMinor / 2));
						this.submitFocusAreaRect(touchRect);
					}
					break;
				default:
					System.Console.WriteLine("Other touch event");
					break;
			}
		}

		private void submitFocusAreaRect(Rect touchRect)
		{
			float[] distances = new float[3];
			Android.Hardware.Camera.Parameters cameraParameters = camera.GetParameters();
			if (cameraParameters.MaxNumFocusAreas == 0)
			{
				return;
			}
			System.Console.WriteLine(surfaceWidth);
			System.Console.WriteLine(surfaceHeight);
			Rect focusArea = new Rect();
			//focusArea.Set(touchRect.Left * 2000 / surfaceWidth - 1000, touchRect.Top * 2000 / surfaceHeight - 1000, touchRect.Right * 2000 / surfaceWidth - 1000, touchRect.Bottom * 2000 / surfaceHeight - 1000);
			focusArea = touchRect;
			//IList<Android.Hardware.Camera.Area> focusAreas = new IList<Android.Hardware.Camera.Area>();
			List<Android.Hardware.Camera.Area> focusAreas = new List<Android.Hardware.Camera.Area>();
			focusAreas.Add(new Android.Hardware.Camera.Area(focusArea, 1000));

			cameraParameters.FocusMode = Android.Hardware.Camera.Parameters.FocusModeMacro;
			cameraParameters.FocusAreas = focusAreas;
			camera.SetParameters(cameraParameters);
			camera.AutoFocus(this);
			cameraParameters.GetFocusDistances(distances);
			System.Console.WriteLine(distances[0]);
			System.Console.WriteLine(distances[1]);
			System.Console.WriteLine(distances[2]);
		}

		void InitializeSensorManager()
		{
			mSensorManager = (SensorManager)GetSystemService(SensorService);
		}

		void ISensorEventListener.OnSensorChanged(SensorEvent e)
		{
			lock (_syncLock)
			{
				//System.Console.WriteLine("Orientation: " + e.Values[0]);
				//ReportDetail.compass = "" + e.Values[0];
				ReportDetail.compass = e.Values[0].ToString(CultureInfo.InvariantCulture);
			}
			//switch (e.Sensor.GetType())
			//{
			//	case Sensor.StringTypeMagneticField:
			//		mags = e.Values;
			//		break;
			//	case Sensor.StringTypeAccelerometer:
			//		accels = e.Values;
			//		break;
			//}

			//if (mags != null && accels != null)
			//{
			//	gravity = new float[9];
			//	magnetic = new float[9];
			//	SensorManager.GetRotationMatrix(gravity, magnetic, accels, mags);
			//	float[] outGravity = new float[9];
			//	SensorManager.RemapCoordinateSystem(gravity,SensorManager.
			//}
		}

		void ISensorEventListener.OnAccuracyChanged(Sensor sensor, SensorStatus accuracy) { }


	}
}

