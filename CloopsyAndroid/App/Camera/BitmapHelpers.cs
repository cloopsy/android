﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System;
namespace CloopsyAndroid
{
	using System.IO;

	using Android.Graphics;
	using Android.Media;
	using Java.IO;
	using Java.Nio;

	public static class BitmapHelpers
	{
		public static Bitmap LoadAndResizeBitmap(this string fileName, int width, int height)
		{
			// First we get the the dimensions of the file on disk
			BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
			BitmapFactory.DecodeFile(fileName, options);

			// Next we calculate the ratio that we need to resize the image by
			// in order to fit the requested dimensions.
			int outHeight = options.OutHeight;
			int outWidth = options.OutWidth;
			int inSampleSize = 1;

			if (outHeight > height || outWidth > width)
			{
				inSampleSize = outWidth > outHeight
								   ? outHeight / height
								   : outWidth / width;
			}

			// Now we will load the image and have BitmapFactory resize it for us.
			options.InSampleSize = inSampleSize;
			options.InJustDecodeBounds = false;
			Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);




			//switch (orientation)
			//{
			//	case "6": //portrait
			//		mtx.PreRotate(90);
			//		resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, mtx, false);
			//		mtx.Dispose();
			//		mtx = null;
			//		break;
			//	case "1": //landscape
			//		break;
			//	default:
			//		mtx.PreRotate(90);
			//		resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, mtx, false);
			//		mtx.Dispose();
			//		mtx = null;
			//		break;

			//}

			return resizedBitmap;
		}

		public static Bitmap FixRotation(string fileName)
		{
			//https://forums.xamarin.com/discussion/5409/photo-being-saved-in-landscape-not-portrait
			Bitmap imageFile = BitmapFactory.DecodeFile(fileName);
			Matrix mtx = new Matrix();
			ExifInterface exif = new ExifInterface(fileName);
			//string orientation = exif.GetAttribute(ExifInterface.TagOrientation);
			var orientation = (Orientation)exif.GetAttributeInt(ExifInterface.TagOrientation, (int)Orientation.Undefined);

			switch (orientation)
			{
				case Orientation.Undefined: // Nexus 7 landscape...
					break;
				case Orientation.Normal: // landscape
					break;
				case Orientation.FlipHorizontal:
					break;
				case Orientation.Rotate180:
					mtx.PreRotate(180);
					imageFile = Bitmap.CreateBitmap(imageFile, 0, 0, imageFile.Width, imageFile.Height, mtx, false);
					mtx.Dispose();
					mtx = null;
					break;
				case Orientation.FlipVertical:
					break;
				case Orientation.Transpose:
					break;
				case Orientation.Rotate90: // portrait
					mtx.PreRotate(90);
					imageFile = Bitmap.CreateBitmap(imageFile, 0, 0, imageFile.Width, imageFile.Height, mtx, false);
					mtx.Dispose();
					mtx = null;
					break;
				case Orientation.Transverse:
					break;
				case Orientation.Rotate270: // might need to flip horizontally too...
					mtx.PreRotate(270);
					imageFile = Bitmap.CreateBitmap(imageFile, 0, 0, imageFile.Width, imageFile.Height, mtx, false);
					mtx.Dispose();
					mtx = null;
					break;
				default:
					mtx.PreRotate(90);
					imageFile = Bitmap.CreateBitmap(imageFile, 0, 0, imageFile.Width, imageFile.Height, mtx, false);
					mtx.Dispose();
					mtx = null;
					break;
			}
			var filePath = System.IO.Path.Combine(fileName);
			var stream = new FileStream(filePath, FileMode.Create);
			imageFile.Compress(Bitmap.CompressFormat.Jpeg, 95, stream);
			stream.Close();

			return imageFile;
			//ReportDetail._file = new FileOutputStream(ReportDetail._dir + "/" + ReportDetail._filename);
			//System.Console.WriteLine(ReportDetail._dir + "/" + ReportDetail._filename);
			//int bytes = imageFile.ByteCount;
			//ByteBuffer buffer = ByteBuffer.Allocate(bytes);
			//imageFile.CopyPixelsToBuffer(buffer);
			//buffer.Rewind();
			//byte[] array = buffer.ToArray<byte>();
			//ReportDetail._file.Write(array);
			//ReportDetail._file.Close();

		}

	}

}
