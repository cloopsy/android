﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

//using System;
//using System.Collections.Generic;
//using Android.App;
//using Android.Content;
//using Android.Content.PM;
//using Android.OS;
//using Android.Provider;
//using Android.Support.V7.App;
//using Android.Widget;
//using Java.IO;
//using Environment = Android.OS.Environment;
//using Uri = Android.Net.Uri;
//using RestLibrary.Models;

//using Cloopsy;
//using Android.Hardware;
//using Android.Graphics;
//using Android.Views;

//namespace CloopsyAndroid
//{
//	[Activity(Label = "Cloopsy Camera", Icon = "@mipmap/icon", Theme = "@style/MyTheme")]
//	public class CameraActivity : AppCompatActivity, ISensorEventListener
//	{
//		private SensorManager mSensorManager;
//		static readonly object _syncLock = new object();


//		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
//		{
//			base.OnActivityResult(requestCode, resultCode, data);

//			// Make it available in the gallery

//			var mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
//			Uri contentUri = Uri.FromFile(ReportDetail._file);
//			mediaScanIntent.SetData(contentUri);
//			SendBroadcast(mediaScanIntent);

//			// TODO
//			//int height = Resources.DisplayMetrics.HeightPixels;
//			//int width = 250;
//			//ReportDetail.bitmap = BitmapHelpers.LoadAndResizeBitmap(ReportDetail._file.Path, width, height);
//			ReportDetail.bitmap = BitmapFactory.DecodeFile(ReportDetail._file.Path);
//			GC.Collect();
//			StartActivity(typeof(CategoryActivity));
//		}

//		protected override void OnCreate(Bundle savedInstanceState)
//		{
//			InitializeSensorManager();

//			base.OnCreate(savedInstanceState);

//			if (IsThereAnAppToTakePictures())
//			{
//				CreateDirectoryForPictures();
//				if (ReportDetail._file == null)
//					TakeAPicture();
//			}
//		}


//		public override void OnBackPressed()
//		{
//			ReportDetail.ReportReset();
//			Finish();
//		}

//		private void CreateDirectoryForPictures()
//		{
//			ReportDetail._dir = new File(
//				Environment.GetExternalStoragePublicDirectory(
//					Environment.DirectoryPictures), "Cloopsy");
//			if (!ReportDetail._dir.Exists())
//			{
//				ReportDetail._dir.Mkdirs();
//			}
//		}

//		private bool IsThereAnAppToTakePictures()
//		{
//			var intent = new Intent(MediaStore.ActionImageCapture);
//			IList<ResolveInfo> availableActivities =
//				PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
//			return availableActivities != null && availableActivities.Count > 0;
//		}


//		private void TakeAPicture()
//		{
//			var intent = new Intent(MediaStore.ActionImageCapture);

//			ReportDetail._file = new File(ReportDetail._dir, string.Format("myPhoto_{0}.jpg", Guid.NewGuid()));

//			intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(ReportDetail._file));

//			StartActivityForResult(intent, 0);
//		}

//		void InitializeSensorManager()
//		{
//			mSensorManager = (SensorManager)GetSystemService(SensorService);
//		}

//		public void OnSensorChanged(SensorEvent e)
//		{
//			lock (_syncLock)
//			{
//				System.Console.WriteLine("Orientation: " + e.Values[0]);
//				ReportDetail.compass = "" + e.Values[0];
//			}
//		}

//		public void OnAccuracyChanged(Sensor sensor, SensorStatus accuracy) { }

//	}

//}

