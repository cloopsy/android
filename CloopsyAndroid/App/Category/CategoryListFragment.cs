﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System.Linq;
using System;
using Android.App;
using Android.OS;
using Newtonsoft.Json.Linq;
using Android.Widget;
using Android.Views;
using Android.Content;

namespace CloopsyAndroid
{
	public class CategoryListFragment : ListFragment
	{
		private int _parentId;
		private JToken _parentCategory;

		public override void OnActivityCreated(Bundle savedInstanceState)
		{
			base.OnActivityCreated(savedInstanceState);
			SetCategoryAdapter();
		}

		//public void SetReportListAdapter()
		//{
		//	Services.ReportService.list(
		//		(res) =>
		//		{
		//			var adapter = new ReportListAdapter(ListView.Context, res);
		//			adapter.OnDeleted += SetReportListAdapter;
		//			this.ListAdapter = adapter;
		//		},
		//		System.Console.WriteLine,
		//		System.Console.WriteLine
		//	);
		//}

		public void SetCategoryAdapter()
		{
			Services.CategoryService.get(
				ReportDetail.parentId,
				(res) =>
				{
					var data = ReportDetail.parentId > -1 ? res["children"] : res;
					_parentId = ReportDetail.parentId;
					_parentCategory = res;
					if (data.Count() > 0)
					{
						var adapter = new CategoryAdapter(ListView.Context, res);
						this.ListAdapter = adapter;
						//adapter.OnClick += SetCategoryAdapter;
						//catListView.Adapter = adapter;
						//catListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
						//{
						//	var obj = data[e.Position];
						//	ReportDetail.parentId = obj["id"].Value<int>();
						//	Console.WriteLine("ID: " + obj["id"]);
						//	Console.WriteLine("Name: " + obj["name"]);
						//	//SetCategoryAdapter(ReportDetail.parentId);
						//	//StartActivity(typeof(CategoryActivity));
						//	//SetCategoryAdapter();
						//	this.Recreate();
						//};

						//var adapter = new ArrayAdapter<JToken>(this, Android.Resource.Layout.SimpleListItem1, (int)data);
						//categoriesListView.Adapter = adapter;
						
					}
					else
					{
						ReportDetail.category = res["id"].ToString();
						ReportDetail.category_name = res["name"].ToString();
						//StartActivity(typeof(ReviewAndSubmitActivity));
						var intent = new Intent(Activity, typeof(ReportEditActivity));
						StartActivity(intent);
					}
				},
				Console.WriteLine,
				Console.WriteLine
			);
		}

		public override void OnListItemClick(ListView l, View v, int position, long id)
		{
			//base.OnListItemClick(l, v, position, id);
			ListView.SetItemChecked(position, true);
			Console.WriteLine(position);
		}

	}
}

