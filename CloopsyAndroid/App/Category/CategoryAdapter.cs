﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using Android.App;
using Android.Content;
using Android.Views;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using RestLibrary.Models;
using Newtonsoft.Json.Linq;
using System.Linq;
using Android.Content.Res;
using Android.Graphics;
using Java.Lang;

namespace CloopsyAndroid
{
	//public delegate void OnClickDelegate(int parentId);
	public delegate void OnItemClickedDelegate(int parentId);
	public class CategoryAdapter : BaseAdapter<object>
	{
		JToken categories;
		private Context mContext;
		public OnItemClickedDelegate OnClick;

		public CategoryAdapter(Context context, JToken categories) : base()
		{
			this.categories = categories;
			this.mContext = context;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override object this[int position]
		{
			get
			{
				return categories[position];
			}
		}

		public override int Count
		{
			get { return categories.Count(); }
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//if (convertView == null)
			//{
			//	var obj = categories[position];
			//	var value = (obj != null ? obj["name"].ToString().ToCharArray() : "".ToCharArray());

			//	convertView = new LinearLayout(mContext);
			//	var view = (LinearLayout)convertView;

			//	//var imageView = new ImageView(context);
			//	//imageView.SetImageDrawable(Resources.Drawable(Resources.Identifier("cloopsy_logo_final", "drawable", context.PackageName)));
			//	//var resID = (int)typeof(Resource.Drawable).GetField("id"+obj["id"].Value<int>()+".jpg").GetValue(null);
			//	//System.Console.WriteLine(resID);
			//	//imageView.SetImageResource(resID);
			//	var labelView = new TextView(mContext);
			//	labelView.SetText(value, 0, value.Length);

			//	labelView.Click += delegate
			//	{
			//		OnClick((obj["id"].Value<int>()));
			//	};

			//	view.AddView(labelView);

			//	return convertView;
			//}
			//return convertView;

			var obj = categories[position];
			var value = (obj != null ? obj["name"].ToString() : "");
			System.Console.WriteLine(value);
			//System.Console.WriteLine("convertView:" + convertView);

			View row = convertView;
			if (row == null)
			{
				//row = LayoutInflater.FromContext(mContext).Inflate(Android.Resource.Layout.ActivityListItem, null);
				row = LayoutInflater.FromContext(mContext).Inflate(Resource.Layout.Category_listview_layout, null,false);
			}
			TextView catName = row.FindViewById<TextView>(Resource.Id.txtCategory);
			ImageView catPic = row.FindViewById<ImageView>(Resource.Id.imgCategory);
			//System.Console.WriteLine("row: " + row);
			try
			{
				//row.FindViewById<TextView>(Android.Resource.Id.Text1).Text = value;
				catName.Text = value;
			}
			catch (Exception e)
			{
				System.Console.WriteLine("Error: " + e);
			}
			System.Console.WriteLine("id" + obj["id"].Value<int>());

			int picId = obj["id"].Value<int>();
			if ((picId == 12) || ((picId > 20) && (picId < 38)) || ((picId > 41) && (picId < 64)))
			{
				var resID = (int)typeof(Resource.Drawable).GetField("id" + obj["id"].Value<int>()).GetValue(null);
				//row.FindViewById<ImageView>(Android.Resource.Id.Icon).SetImageResource(resID);
				catPic.SetImageResource(resID);
			}

			catName.Click += delegate {
				
			};

			return row;

		}
	}
}

