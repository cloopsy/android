﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using Android.App;
using Android.OS;
using Android.Widget;
using System.Linq;
using System;
using Android.Support.V7.App;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CloopsyAndroid
{
	[Activity(Label = "CategoryActivity", Icon = "@mipmap/icon", Theme = "@style/MyTheme")]
	public class CategoryActivity : AppCompatActivity
	{
		private Android.Support.V7.Widget.Toolbar mToolbar;
		private ListView catListView;
		private int _parentId;
		private JToken _parentCategory;
		private CategoryListFragment categoryListFragment;
		private ImageView _catchedPicImageview;
		//List<object> catList = new List<object>();
		//JToken data;
		//bool updateCategories;
		//CategoryAdapter adapter;


		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Category_layout);
			mToolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbarCategory);
			SetSupportActionBar(mToolbar);
			SupportActionBar.SetTitle(Resource.String.CloopsyCategory);

			catListView = FindViewById<ListView>(Resource.Id.categoriesListView);
			//var fragmentTransaction = this.FragmentManager.BeginTransaction();
			//categoryListFragment = new CategoryListFragment();
			//fragmentTransaction.Add(Resource.Id.CategoryListFragmentContainer, categoryListFragment);
			//fragmentTransaction.Commit();

			_catchedPicImageview = FindViewById<ImageView>(Resource.Id.catchedPicImageview);
			if (ReportDetail.bitmap != null)
				_catchedPicImageview.SetImageBitmap(ReportDetail.bitmap);

			Console.WriteLine("ReportDetail parentID: " + ReportDetail.parentId);
			//SetCategoryAdapter(ReportDetail.parentId);
			//updateCategories = GetCategoriesFromServer();
			//Console.WriteLine(data);
			SetCategoryAdapter();
		}

		//public bool GetCategoriesFromServer()
		//{
		//	bool updateList = true;
		//	Services.CategoryService.get(
		//		ReportDetail.parentId,
		//		(res) =>
		//		{
		//			data = ReportDetail.parentId > -1 ? res["children"] : res;
		//			_parentId = ReportDetail.parentId;
		//			_parentCategory = res;
		//			if (data.Count() > 0)
		//			{
		//				updateList = true;
		//			}
		//			else
		//			{
		//				updateList = false;
		//				//ReportDetail.category = res["id"].ToString();
		//				//ReportDetail.category_name = res["name"].ToString();
		//				//StartActivity(typeof(ReviewAndSubmitActivity));
		//			}
		//		},
		//		Console.WriteLine,
		//		Console.WriteLine
		//	);
		//	Console.WriteLine(updateList);
		//	return updateList;
		//}

		public void SetCategoryAdapter()
		{
			Services.CategoryService.get(
				ReportDetail.parentId,
				(res) =>
				{
					var data = ReportDetail.parentId > -1 ? res["children"] : res;
					_parentId = ReportDetail.parentId;
					_parentCategory = res;
					if (data.Count() > 0)
					{
						CategoryAdapter adapter = new CategoryAdapter(this, data);
						//adapter.OnClick += SetCategoryAdapter;
						catListView.Adapter = adapter;
						catListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
						{
							var obj = data[e.Position];
							ReportDetail.parentId = obj["id"].Value<int>();
							Console.WriteLine("ID: " + obj["id"]);
							Console.WriteLine("Name: " + obj["name"]);
							//SetCategoryAdapter(ReportDetail.parentId);
							//StartActivity(typeof(CategoryActivity));
							//SetCategoryAdapter();
							this.Recreate();
							//adapter.NotifyDataSetChanged();
						}; 
						//var adapter = new ArrayAdapter<JToken>(this, Android.Resource.Layout.SimpleListItem1, (int)data);
						//categoriesListView.Adapter = adapter;
					}
					else
					{
						ReportDetail.category = res["id"].ToString();
						ReportDetail.category_name = res["name"].ToString();
						StartActivity(typeof(ReviewAndSubmitActivity));
					}
				},
				Console.WriteLine,
				Console.WriteLine
			);
		}

		//public void SetCategoryAdapter()
		//{
		//	CategoryAdapter adapter = new CategoryAdapter(this, data);
		//	catListView.Adapter = adapter;
		//	catListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
		//	{
		//		if (updateCategories == true)
		//		{
		//			var obj = data[e.Position];
		//			ReportDetail.parentId = obj["id"].Value<int>();
		//			updateCategories = GetCategoriesFromServer();
		//			adapter.NotifyDataSetChanged();
		//		}
		//		else
		//		{
		//			ReportDetail.category = _parentCategory["id"].ToString();
		//			ReportDetail.category_name = _parentCategory["name"].ToString();
		//			StartActivity(typeof(ReviewAndSubmitActivity));
		//		}
		//	};
		//}

		public override void OnBackPressed()
		{
			Console.WriteLine("Back pressed -> parentId: " + ReportDetail.parentId);
			if (ReportDetail.parentId > -1)
			{
				Console.WriteLine("Back pressed -> parentId: " + ReportDetail.parentId);
				var id = _parentCategory["parent"].Value<string>();
				//SetCategoryAdapter(id != null ? int.Parse(id) : -1);
				ReportDetail.parentId = (id != null ? int.Parse(id) : -1);
				//this.Recreate();
				StartActivity(typeof(CategoryActivity));
				//updateCategories = GetCategoriesFromServer();
				//adapter.NotifyDataSetChanged();
			}
			else
				ReportDetail.ReportReset();
				base.OnBackPressed();
		}
	}
}



