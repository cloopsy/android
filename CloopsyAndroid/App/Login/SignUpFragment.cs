﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;

namespace CloopsyAndroid
{
	public class OnSignUpEventArgs : EventArgs
	{
		private string mFirstName;
		private string mEmail;
		private string mPassword;

		public string FirstName
		{
			get { return mFirstName; }
			set { mFirstName = value; }
		}
		public string Email
		{
			get { return mEmail; }
			set { mEmail = value; }
		}
		public string Password
		{
			get { return mPassword; }
			set { mPassword = value; }
		}

		public OnSignUpEventArgs(string firstname, string email, string password) : base()
		{
			FirstName = firstname;
			Email = email;
			Password = password;
		}
	}


	public class dialog_SignUp : DialogFragment
	{
		private EditText mTxtFirstName;
		private EditText mTxtEmail;
		private EditText mTxtPassword;
		private Button mBtnSignUp;

		public event EventHandler<OnSignUpEventArgs> mOnSignUpComplete;

		public dialog_SignUp()
		{
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView(inflater, container, savedInstanceState);

			var view = inflater.Inflate(Resource.Layout.SignUp_layout, container, false);
			mTxtFirstName = view.FindViewById<EditText>(Resource.Id.txtFirstName);
			mTxtEmail = view.FindViewById<EditText>(Resource.Id.txtEmail);
			mTxtPassword = view.FindViewById<EditText>(Resource.Id.txtPassword);
			mBtnSignUp = view.FindViewById<Button>(Resource.Id.btnDialogEmail);

			mBtnSignUp.Click += MBtnSignUp_Click;
			return view;
		}

		void MBtnSignUp_Click(object sender, EventArgs e)
		{
			//User has clicked the SignUp button
			mOnSignUpComplete.Invoke(this, new OnSignUpEventArgs(mTxtFirstName.Text, mTxtEmail.Text, mTxtPassword.Text));
			this.Dismiss();
		}

		public override void OnActivityCreated(Bundle savedInstanceState)
		{
			Dialog.Window.RequestFeature(WindowFeatures.NoTitle); //Sets the title bar to invisible
			base.OnActivityCreated(savedInstanceState);
			//Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation; //set the animation
		}
	}
}


