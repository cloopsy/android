﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;

namespace CloopsyAndroid
{
	public class OnSignInEventArgs : EventArgs
	{
		private string mEmail;
		private string mPassword;

		public string Email
		{
			get { return mEmail; }
			set { mEmail = value; }
		}
		public string Password
		{
			get { return mPassword; }
			set { mPassword = value; }
		}

		public OnSignInEventArgs(string email, string password) : base()
		{
			Email = email;
			Password = password;
		}
	}


	public class dialog_SignIn : DialogFragment
	{
		private EditText mTxtEmail;
		private EditText mTxtPassword;
		private Button mBtnSignIn;

		public event EventHandler<OnSignInEventArgs> mOnSignInComplete;

		public dialog_SignIn()
		{
		}

		public override Android.Views.View OnCreateView(Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView(inflater, container, savedInstanceState);

			var view = inflater.Inflate(Resource.Layout.SigIn_dialog, container, false);
			mTxtEmail = view.FindViewById<EditText>(Resource.Id.txtEmail);
			mTxtPassword = view.FindViewById<EditText>(Resource.Id.txtPassword);
			mBtnSignIn = view.FindViewById<Button>(Resource.Id.btnDialogSignIn);

			mBtnSignIn.Click += MBtnSignIn_Click;
			return view;
		}

		void MBtnSignIn_Click(object sender, EventArgs e)
		{
			//User has clicked the SignIn button
			mOnSignInComplete.Invoke(this, new OnSignInEventArgs(mTxtEmail.Text, mTxtPassword.Text));
			this.Dismiss();
		}

		public override void OnActivityCreated(Bundle savedInstanceState)
		{
			Dialog.Window.RequestFeature(WindowFeatures.NoTitle); //Sets the title bar to invisible
			base.OnActivityCreated(savedInstanceState);
			//Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation; //set the animation
		}
	}
}

