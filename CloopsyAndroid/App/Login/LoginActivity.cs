﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace CloopsyAndroid
{
	[Activity(Label = "Cloopsy Login", Icon = "@mipmap/icon", Theme = "@style/MyTheme", NoHistory = true)]
	public class LoginActivity : AppCompatActivity
	{
		private Button mBtnSignUp;
		private Button mBtnSignIn;
		private ProgressBar mProgressBar;
		private Android.Support.V7.Widget.Toolbar mToolbar;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);
			mBtnSignIn = FindViewById<Button>(Resource.Id.btnSgnIn);
			mBtnSignUp = FindViewById<Button>(Resource.Id.btnSgnUp);
			mProgressBar = FindViewById<ProgressBar>(Resource.Id.progressBar1);
			mToolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			SetSupportActionBar(mToolbar);
			//SupportActionBar.Title = Resource.String.CloopsyLogin;
			SupportActionBar.SetTitle(Resource.String.CloopsyLogin);

			mBtnSignUp.Click += (object sender, EventArgs args) =>
			{
				//Pull Up dialog
				//FragmentTransaction transaction = FragmentManager.BeginTransaction();
				//dialog_SignUp signUpDialog = new dialog_SignUp();
				//signUpDialog.Show(transaction, "dialog fragment");

				//signUpDialog.mOnSignUpComplete += signUpDialog_mOnSignUpComplete;

				//https://developer.xamarin.com/recipes/android/fundamentals/intent/open_a_webpage_in_the_browser_application/
				var uri = Android.Net.Uri.Parse("https://cloopsy.unipv.it/accounts/signup/");
				var intent = new Intent(Intent.ActionView, uri);
				StartActivity(intent);
			};

			mBtnSignIn.Click += (object sender, EventArgs e) => 
			{
				FragmentTransaction transaction = FragmentManager.BeginTransaction();
				dialog_SignIn signInDialog = new dialog_SignIn();
				signInDialog.Show(transaction, "dialog fragment");

				signInDialog.mOnSignInComplete += this.signInDialog_mOnSignInComplete;
			};

		}

		public void signInDialog_mOnSignInComplete(object sender, OnSignInEventArgs e)
		{
			Console.WriteLine(e.Email);
			Console.WriteLine(e.Password);
			var authService = new RestLibrary.Services.AuthManager();
			authService.login(
				new Dictionary<string, object>()
				{
					{"email", e.Email},
					{"password", e.Password}
				},
				(res) =>
				{
					Console.WriteLine(res);
					StartActivity(typeof(TutorialActivity));
				},
				(err) =>
				{
					var builder = new Android.Support.V7.App.AlertDialog.Builder(this);
					var alertDialog = builder.Create();
					Console.WriteLine(err);
					alertDialog.SetTitle("Login Failed");
					alertDialog.SetMessage(err);
					alertDialog.Show();
				});
		}

		void signUpDialog_mOnSignUpComplete(object sender, OnSignUpEventArgs e)
		{
			mProgressBar.Visibility = ViewStates.Visible;
			Thread thread = new Thread(ActLikeARequest);
			thread.Start();
		}


		private void ActLikeARequest()
		{
			Thread.Sleep(3000);
			RunOnUiThread(() => { mProgressBar.Visibility = ViewStates.Invisible; });
		}


	}
}


