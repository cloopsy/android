﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/
using System;
using Android.App;
using Android.Media;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace CloopsyAndroid
{
	[Activity(Label = "Cloopsy Tutorial", Icon = "@mipmap/icon", Theme = "@style/MyTheme", NoHistory = true)]
	public class TutorialActivity : AppCompatActivity,MediaPlayer.IOnPreparedListener,Android.Views.ISurfaceHolderCallback
	{
		private Button mBtnNext;
		private VideoView videoView;
		MediaPlayer mediaPlayer;
		private Android.Support.V7.Widget.Toolbar mToolbar;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.Tutorial_layout);
			mBtnNext = FindViewById<Button>(Resource.Id.btnNext);
			videoView = FindViewById<VideoView>(Resource.Id.tutorialVideoView);
			mToolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			SetSupportActionBar(mToolbar); 
			SupportActionBar.SetTitle(Resource.String.CloopsyTutorial);
			mBtnNext.Enabled = false;

			//http://stackoverflow.com/questions/26466191/play-video-in-android-using-xamarin
			ISurfaceHolder holder = videoView.Holder;
			holder.SetType(SurfaceType.PushBuffers);
			holder.AddCallback(this);

			var descriptor = Assets.OpenFd("Cloopsy_tutorial_540_960.mp4");
			mediaPlayer = new MediaPlayer();
			mediaPlayer.SetDataSource(descriptor.FileDescriptor, descriptor.StartOffset, descriptor.Length);
			mediaPlayer.Prepare();
			mediaPlayer.Looping = false;
			mediaPlayer.Completion += delegate {
				mBtnNext.Enabled = true;
			};
			mediaPlayer.Start();

			//var uri = Android.Net.Uri.Parse("android.resource://CloopsyAndroid/" + Resource.Drawable.Cloopsy_tutorial_test_conv);
			//videoView.SetMediaController(new MediaController(this));
			//videoView.SetVideoURI(uri);
			//videoView.Start();

			mBtnNext.Click += delegate {
				RestLibrary.Settings.TutorialDone = true;
				StartActivity(typeof(HomeActivity));	
			}; 
		}

		public void SurfaceCreated(ISurfaceHolder holder)
		{
			Console.WriteLine("Surface Created");
			mediaPlayer.SetDisplay(holder);
		}

		public void SurfaceDestroyed(ISurfaceHolder holder)
		{
			Console.WriteLine("Surface Destroyed");
		}

		public void SurfaceChanged(ISurfaceHolder holder, Android.Graphics.Format format, int w, int h)
		{
			Console.WriteLine("Surface Changed");
		}

		public void OnPrepared(MediaPlayer mediaPlayer)
		{
			
		}
	}
}

