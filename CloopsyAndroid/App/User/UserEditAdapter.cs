﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using Android.Content;
using Android.Views;
using Android.Widget;
using RestLibrary.Models;
using System;
using System.Collections.Generic;

namespace CloopsyAndroid
{
	public class UserEditAdapter : BaseAdapter<object>
	{

		User user;
		Context context;


		public UserEditAdapter(Context context, User user) : base()
		{
			this.user = user;
			this.context = context;
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override object this[int position]
		{
			get
			{
				return user.GetType().GetProperty(GetItemPropriety(position)).GetValue(user);
			}
		}

		public override int Count
		{
			get { return User.FIELDS.Length; }
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				var prop = GetItemPropriety(position);
				var obj = this[position];
				var value = (obj != null ? obj.ToString().ToCharArray() : "".ToCharArray());

				convertView = new LinearLayout(context);
				var view = (LinearLayout)convertView;

				var labelView = new TextView(context);
				labelView.SetText(prop.ToCharArray(), 0, prop.Length);

				TextView valueView;

				if (Array.IndexOf(User.READ_ONLY_FIELD, prop) > -1)
				{
					valueView = new TextView(context);
					valueView.SetText(value, 0, value.Length);
				}
				else
				{
					valueView = new EditText(context);
					valueView.InputType = Android.Text.InputTypes.TextVariationShortMessage;
					valueView.SetEms(15);
					valueView.SetText(value, 0, value.Length);
				}

				view.AddView(labelView);
				view.AddView(valueView);

				return convertView;
			}
			return convertView;
		}

		public static Dictionary<string, object> getDataFromView(ListView view)
		{
			var output = new Dictionary<string, object>();
			for (int index = 0; index < view.ChildCount; ++index)
			{
				var nextChild = (LinearLayout)view.GetChildAt(index);
				var valueView = (TextView)nextChild.GetChildAt(1);
				var text = valueView.Text;
				var label = GetItemPropriety(index);
				output.Add(label, text);
			}
			return output;
		}

		public static string GetItemPropriety(int position)
		{
			return User.FIELDS[position];
		}
	}
}

