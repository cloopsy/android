﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using Android.App;
using Android.Content;
using Android.Support.V7.App;
using Android.Webkit;
using Java.Lang;
using Java.Util;
using Org.Apache.Http.Client.Methods;
using Org.Apache.Http.Impl.Client;

namespace CloopsyAndroid
{
	[Activity(Label = "Cloopsy WebGIS", Icon = "@mipmap/icon", Theme = "@style/MyTheme", NoHistory = true)]
	public class WebgisActivity: AppCompatActivity
	{
		WebView webview;
		private Android.Support.V7.Widget.Toolbar mToolbar;

		protected override void OnCreate(Android.OS.Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Webgis_layout);

			mToolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			SetSupportActionBar(mToolbar);
			//SupportActionBar.SetTitle();

			webview = FindViewById<WebView>(Resource.Id.webview);
			webview.Settings.JavaScriptEnabled = true;
			System.String url = "https://cloopsy.unipv.it/dist/webgis-mobile.html?token=" + RestLibrary.Settings.LOGINKEY;
			Console.WriteLine("URL: " + url);
			webview.LoadUrl(url);
		}

		protected override void OnPause()
		{
			base.OnPause();
		}

		protected override void OnResume()
		{
			base.OnResume();
		}

		public override void OnLowMemory()
		{
			base.OnLowMemory();
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
		}

		//protected override void OnSaveInstanceState(Bundle outState)
		//{
		//	base.OnSaveInstanceState(outState);
		//	mapView.OnSaveInstanceState(outState);
		//}


	}


	//public class MyWebViewClient : WebViewClient
	//{
	//	private Context context;

	//	public MyWebViewClient(Context context)
	//	{
	//		this.context = context;
	//	}

	//	public override bool ShouldOverrideUrlLoading(WebView view, string url) // called when links in the webview are selected
	//	{
	//		Console.WriteLine("ShouldOverrideUrlLoading: " + url);
	//		Dictionary<string, string> headers = new Dictionary<string, string>();
	//		headers.Add("Authorization: Token ", RestLibrary.Settings.LOGINKEY);
	//		Console.WriteLine("ShouldOverrideUrlLoading: " + "Authorization: Token " + RestLibrary.Settings.LOGINKEY);
	//		view.LoadUrl(url, headers);
	//		return true;
	//	}

	//	public override WebResourceResponse ShouldInterceptRequest(WebView wv, string url)
	//	{
	//		//if (url.Length > 5 && (url.Substring(url.Length - 5, 5) == ".com/" || url.Substring(url.Length - 3, 3) == "php"))
	//			//return null;
	//		//base.ShouldInterceptRequest(wv, url);
	//		//HttpWebRequest request = new HttpWebRequest(new System.Uri(url));
	//		//WebHeaderCollection whc = new WebHeaderCollection();
	//		//whc.Set(HttpRequestHeader.KeepAlive, "true");
	//		//request.Headers = whc;
	//		//System.Console.WriteLine("Headers: {0}", request.Headers.ToString());
	//		//HttpWebResponse response = request.GetResponse() as HttpWebResponse;

	//		//Stream responseStream = response.GetResponseStream();

	//		//return new WebResourceResponse(response.ContentType, response.ContentEncoding, responseStream);
	//		return base.ShouldInterceptRequest(wv, url);
	//	}

	//	public override void OnLoadResource(WebView view, string url) // called for resources--but can't append header!
	//	{
	//		Console.WriteLine("OnLoadResource: " + url);
	//		base.OnLoadResource(view, url);
	//	}
	//}


}
