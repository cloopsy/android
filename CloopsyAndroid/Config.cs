﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.Net;
using RestLibrary.Services;

namespace CloopsyAndroid
{
	public static class Settings
	{
		public static bool isTest = false;
	}
	public static class Services
	{
		public static AuthManager AuthService;
		public static UserManager UserService;
		public static ReportManager ReportService;
		public static CategoryManager CategoryService;
		public static bool isLogged = true;

		public static bool Bootstrap(Context context)
		{
			//AuthService = new AuthManager();
			//UserService = new UserManager();
			//ReportService = new ReportManager(GetConnection, IsThereConnection);
			//CategoryService = new CategoryManager();

			//NotValidKeyDelegate notValidKey = delegate
			//{
			//	context.StartActivity(typeof(LoginActivity));
			//};

			//AuthService.notValidKey = notValidKey;
			//UserService.notValidKey = notValidKey;
			//ReportService.notValidKey = notValidKey;
			//CategoryService.notValidKey = notValidKey;
			AuthService = new AuthManager();
			UserService = new UserManager();
			ReportService = new ReportManager(GetConnection, IsThereConnection);
			RestLibrary.Settings.ISONLINE = IsThereConnection();
			//RestLibrary.Settings.TutorialDone = false;
			//Console.WriteLine(RestLibrary.Settings.ISONLINE);
			CategoryService = new CategoryManager();

			NotValidKeyDelegate notValidKey = delegate
			{
				isLogged = false;
			};

			AuthService.notValidKey = notValidKey;
			UserService.notValidKey = notValidKey;
			ReportService.notValidKey = notValidKey;
			CategoryService.notValidKey = notValidKey;

			return isLogged;
		}

		public static bool IsThereConnection()
		{
			ConnectivityManager cm = (ConnectivityManager)Application.Context.GetSystemService(Context.ConnectivityService);
			NetworkInfo netInfo = cm.ActiveNetworkInfo;
			bool isReachable = (netInfo != null) && netInfo.IsConnected;
			return isReachable;
			//return netInfo != null && netInfo.isConnectedOrConnecting();
			//return false;

		}

		public static SQLite.SQLiteConnection GetConnection()
		{
			var sqliteFilename = "cloopsy.db3";
			string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // Documents folder
			var path = Path.Combine(documentsPath, sqliteFilename);
			// Create the connection
			var conn = new SQLite.SQLiteConnection(path);
			// Return the database connection
			return conn;
		}
	}
}
