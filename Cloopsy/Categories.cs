﻿using System;
using System.Collections.Generic;

namespace Cloopsy
{
	public static class Categories
	{
		public static List<string> MacroCategory = new List<string>() {"Artificial surfaces","Agricultural areas","Forests and semi-natural areas","Wetlands","Water bodies" };

		public static List<string> CategoryArtificial = new List<string>() {"Urban fabric","Industrial, commercial and transport units","Mine, dump and construction sites","Artificial non-agricultural vegetated areas" };
		public static List<string> CategoryAgricultural = new List<string>() { "Arable land","Permanent crops","Pastures","Heterogeneous agricultural areas"};
		public static List<string> CategoryForest = new List<string>() { "Forests","Shrub and/or herbaceous vegetation associations","Open spaces with little or no vegetation"};
		public static List<string> CategoryWetlands = new List<string>() { "Inland wetlands","Coastal wetlands"};
		public static List<string> CategoryWater = new List<string>() { "Inland waters","Marine waters"};

		public static List<string> SubCategoryUrban = new List<string>() { "Continuous urban fabric","Discountinuous urban fabric"};
		public static List<string> SubCategoryIndustrial = new List<string>() { "Industrial or commercial units","Road and rail networks and associated land","Port areas","Airports"};
		public static List<string> SubCategoryMine = new List<string>() { "Mineral extraction sites","Dump sites","Construction sites"};
		public static List<string> SubCategoryNonAgricultural = new List<string>() { "Green urban areas","Sport and leisure facilities"};

		public static List<string> SubCategoryArable = new List<string>() { "Non-irrigated arable land", "Permanently irrigated land","Rice fields" };
		public static List<string> SubCategoryPermanent = new List<string>() { "Vineyards", "Fruit trees and berry plantations","Olive groves" };
		public static List<string> SubCategoryHeterogeneous = new List<string>() { "Annual crops associated with permanent crops", "Complex cultivation patterns","Land principally occupied by agriculture, with significant areas of natural vegetation","Agro-forestry areas" };

		public static List<string> SubCategoryForest = new List<string>() { "Broad-leaved forest", "Coniferous forest","Mixed forest" };
		public static List<string> SubCategoryShrub = new List<string>() { "Natural grassland", "Moors and heatland","Sclerophyllous vegetation","Transitional woodland/shrub" };
		public static List<string> SubCategoryOpen = new List<string>() { "Beaches, dunes, sands", "Bare rock","Sparsely vegetated areas","Burnt areas","Glaciers and perpetual snow" };

		public static List<string> SubCategoryInland = new List<string>() { "Inland marshes", "Peatbogs" };
		public static List<string> SubCategoryCoastal = new List<string>() { "Salt marshes", "Salines","Interdial flats" };

		public static List<string> SubCategoryInlandWat = new List<string>() { "Water courses", "Water bodies" };
		public static List<string> SubCategoryMarine = new List<string>() { "Coastal lagoons", "Estuaries", "Sea and ocean" };
	}
}

