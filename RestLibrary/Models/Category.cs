﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

namespace RestLibrary.Models
{
	public class Category
	{
		public int Picture { get; set; }
		public string Name { get; set; }

		public Category(int picture, string name)
		{
			this.Picture = picture;
			this.Name = name;
		}
	}
	public class CategoryModel
	{
		public static string[] FIELDS = {"id", "name", "selectable", "parent", "children"};
		public int id { get; set; }
		public string name { get; set; }
		public bool selectable { get; set; }
		public int parent { get; set; }
		public CategoryModel[] children { get; set; }
	}
}
