﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using SQLite;

namespace RestLibrary.Models
{
	public class Report<FileType>
	{
		public static string[] FIELDS = { "id", "category", "position", "direction", "distance_focus", "telephone_id", "image", "valid", "created_at" };
		public static string[] READ_ONLY_FIELD = { "id", "position", "direction", "distance_focus", "telephone_id", "image", "valid", "created_at" };
		public int id { get; set; }
		public string category { get; set; }
		public string position { get; set; }
		public string direction { get; set; }
		public string distance_focus { get; set; }
		public string telephone_id { get; set; }
		public FileType image { get; set; }
		public string valid { get; set; }
		public string created_at{ get; set; }
	}
	[Table("report")]
	public class ReportOffline
	{
		public static string[] FIELDS = { "id", "category", "position", "direction", "distance_focus", "telephone_id", "image", "valid", "created_at" };
		public static string[] READ_ONLY_FIELD = { "id", "position", "direction", "distance_focus", "telephone_id", "image", "valid", "created_at" };
		int _id;
		[Column("id")]
		[PrimaryKey]
		[NotNull]
		[AutoIncrement]
		public int id { get; set; }
		string _category;
		[Column("category")]
		public string category { get; set; }
		string _position;
		[Column("position")]
		public string position { get; set; }
		string _direction;
		[Column("category")]
		public string direction { get; set; }
		string _distance_focus;
		[Column("distance_focus")]
		public string distance_focus { get; set; }
		string _telephone_id;
		[Column("telephone_id")]
		public string telephone_id { get; set; }
		string _image;
		[Column("image")]
		public string image { get; set; }
		string _valid;
		[Column("valid")]
		public string valid { get; set; }
		string _created_at;
		[Column("created_at")]
		public string created_at { get; set; }
	}
}