﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

namespace RestLibrary.Models
{
	public class Auth
	{
		public string email { get; set; }
		public string password { get; set; }
		public string password1 { get; set; }
		public string password2 { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string address { get; set; }
		public string telephone { get; set; }
		public int age { get; set; }
		public string organization { get; set; }
		public string non_field_errors { get; set; }
	}
}
