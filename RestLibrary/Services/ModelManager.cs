﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using System.Collections.Generic;

namespace RestLibrary.Services
{
	public abstract class ModelManager: HttpManager
	{
		public Type SerializerType { protected set; get; }

		public string ApiURL { protected set; get; }

		protected ModelManager (Type serializerType, string apiUrl): base()
		{
			SerializerType = serializerType;
			ApiURL = apiUrl;
		}

		public override string REST_ROOT {
			get
			{
				return string.Format("{0}{1}/", Settings.API_ROOT, ApiURL);
			}
		}

		public void get(
			int id,
		    ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			_sync();
			if (id >= 0)
				_get(id.ToString()+"/", res, err, then);
			else
				get(res, err, then);
		}

		public void get(
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			_sync();
			_get("", res, err, then);
		}


		public void list(
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			_sync();
			_get("", res, err, then);
		}

		public void create(
			Dictionary<string, object> data,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			_sync();
			_post(data, SerializerType, "", res, err, then);
		}

		public void update(
			Dictionary<string, object> data,
			int id,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null
		)
		{
			_sync();
			if (id >= 0)
				_put(data, SerializerType, id.ToString(), res, err, then);
			else
				update(data, res, err, then);
		}

		public void update(
			Dictionary<string, object> data,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null
		)
		{
			_sync();
			_put(data, SerializerType, "", res, err, then);
		}

		public void delete(
			int id,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			_sync();
			_delete(id.ToString()+"/", res, err, then);
		}

		protected void _sync()
		{
		}
			
	}
}
