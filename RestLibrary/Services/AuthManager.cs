﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using System.Collections.Generic;

namespace RestLibrary.Services
{
	public class AuthManager: HttpManager
	{
		public override string REST_ROOT {
			get
			{
				return Settings.AUTH_ROOT;
			}
		}

		public void login(Dictionary<string, object> data, ResponseDelegate res = null, ErrorDelegate err = null, ThenDelegate then = null) {
			Settings.LOGINKEY = string.Empty;
			Settings.EMAIL = string.Empty;
			_post(data, typeof(Serializers.AuthSerializer), "login/", (result) =>
			{
				Settings.LOGINKEY = result["key"].ToString();
				//Settings.EMAIL = result["email"].ToString();
				res(result);
			}, err, then);
		}

		public void logout(ResponseDelegate res = null, ErrorDelegate err = null, ThenDelegate then = null)
		{
			try
			{
				Settings.LOGINKEY = "";
			}
			catch (Exception e)
			{
				err(e.ToString());
			}
			finally
			{
				then();
			}
		}
	}
}
