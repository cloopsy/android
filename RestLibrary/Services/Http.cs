﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System.Net.Http;
using System.Net;
using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using RestLibrary.Serializers;
using System.Net.Http.Headers;

namespace RestLibrary.Services
{
	public class Http
	{
		private static HttpClient _instance;
		public static HttpClient HttpClient
		{
			get
			{
				if (_instance == null)
				{
					_instance = new HttpClient();
				}
				if (Settings.LOGINKEY != "")
					_instance.DefaultRequestHeaders.Authorization =
						new AuthenticationHeaderValue("Token", Settings.LOGINKEY);
				else
					_instance.DefaultRequestHeaders.Authorization = null;
				return _instance;
			}
		}
		private Http() { }
	}
	public abstract class HttpManager
	{
		protected HttpClient _httpClient
		{
			get
			{
				return Http.HttpClient;
			}
		}

		protected HttpManager() { }
			
		public virtual string REST_ROOT
		{
			get
			{
				return "Not Implemented";
			}
		}

		public NotValidKeyDelegate notValidKey = null;

		protected async void _promise(
			string method,
			string uri,
			MultipartFormDataContent formData = null,
			StringContent data = null,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			try
			{
				uri = String.Format("{0}{1}", REST_ROOT, uri);
				HttpResponseMessage responseHttp;
				if ((method == "post" || method == "put") && (data == null && formData == null))
					throw new Exception("method put and get require data");
				switch (method)
				{
					case "post":
						if (data != null)
							responseHttp = await _httpClient.PostAsync(uri, data);
						else
							responseHttp = await _httpClient.PostAsync(uri, formData);
						break;
					case "put":
						if (data != null)
							responseHttp = await _httpClient.PutAsync(uri, data);
						else
							responseHttp = await _httpClient.PutAsync(uri, formData);
						break;
					case "get":
						responseHttp = await _httpClient.GetAsync(uri);
						break;
					case "delete":
						responseHttp = await _httpClient.DeleteAsync(uri);
						break;
					default:
						throw new Exception("availabled methods: post, get, delete, put");	
				}
				string response = await responseHttp.Content.ReadAsStringAsync();
				if (responseHttp.IsSuccessStatusCode && res != null)
				{
					JToken responseJObject = JToken.Parse(response);
					res(responseJObject);
				}
				else if(!responseHttp.IsSuccessStatusCode)
				{
					try
					{
						// check if token is valid.
						if (JObject.Parse(response).GetValue("detail").ToString() == "Invalid token.")
						{
							Settings.LOGINKEY = "";
							if (notValidKey != null)
								notValidKey();
						}
						else
							err(string.Format("Error: {0}", JObject.Parse(response).GetValue("detail").ToString()));
					}
					catch
					{
						err(string.Format("Error: {0}", response.ToString()));
					}
				}
			}
			catch (Exception e)
			{
				if (err != null)
					err(string.Format("Error: {0}", e.ToString()));
			}
			finally
			{
				if (then != null)
					then();
			}
		}

		protected void _post(
			Dictionary<string, object> data,
			Type serializerType,
			string uri,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			var d = (Activator.CreateInstance(serializerType, data) as IBaseSerializer).FormData;
			this._promise("post", uri, d, res: res, err: err, then: then);
		}

		protected void _put(
			Dictionary<string, object> data,
			Type serializerType,
			string uri,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			var d = (Activator.CreateInstance(serializerType, data) as IBaseSerializer).FormData;
			this._promise("put", uri, d, res: res, err: err, then: then);
		}

		protected void _get(
			string uri,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			_promise("get", uri, null, res: res, err: err, then: then);
		}

		protected void _delete(
			string uri,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			_promise("delete", uri, null, res: res, err: err, then: then);
		}
	}
	public delegate void ResponseDelegate(JToken response);
	public delegate void ErrorDelegate(string error);
	public delegate void ThenDelegate();
	public delegate void NotValidKeyDelegate();
}

