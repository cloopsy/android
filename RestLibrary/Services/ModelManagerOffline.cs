﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using System.Collections.Generic;
using RestLibrary.Serializers;
using RestLibrary.Services;
using SQLite;

namespace RestLibrary
{
	public delegate SQLiteConnection GetOfflineConnectionDelegate();
	public delegate bool GetIfConnectionDelegate();
	public abstract class ModelManagerOffline<T>: ModelManager where T : new()
	{
		protected SQLiteConnection _offlineConnection;
		public GetIfConnectionDelegate IsThereConnection { get; set; }
		public Type SerializerTypeOffline { protected set; get; }

		public ModelManagerOffline(
			Type serializerType,
			Type serializerTypeOffline,
            string apiUrl,
            GetOfflineConnectionDelegate connDelegate,
          	GetIfConnectionDelegate isThereConnection): base(serializerType, apiUrl)
		{
			_offlineConnection = connDelegate();
			SerializerTypeOffline = serializerTypeOffline;
			IsThereConnection = isThereConnection;
			setTable();
		}

		public void setTable()
		{
			_offlineConnection.CreateTable<T>();
		}

		public void createOffline(Dictionary<string, object> data)
		{
			var obj = ((BaseSerializer)Activator.CreateInstance(SerializerType, data)).Model;
			_offlineConnection.Insert(obj);
		}

		public void deleteOffline(int id)
		{
			_offlineConnection.Delete<T>(id);
		}

		public void getOffline(int id)
		{
			_offlineConnection.Get<T>(id);
		}

		public IEnumerable<T> listOffline()
		{
			return _offlineConnection.Table<T>();
		}

		public void updateOffline(Dictionary<string, object> data, int id)
		{
			var obj = ((BaseSerializer)Activator.CreateInstance(SerializerType, data)).Model;
			_offlineConnection.Update(obj);
		}


		public new void get(
			int id,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			if (IsThereConnection())
			{
				base.get(id, res, err, then);
			}
			else
			{
				getOffline(id);
			}
		}

			
		public new void list(
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			if (IsThereConnection())
			{
				base.list(res, err, then);
			}
			else
			{
				listOffline();
			}
		}


		public new void create(
			Dictionary<string, object> data,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			if (IsThereConnection())
			{
				base.create(data, res, err, then);
			}
			else
			{
				createOffline(data);
			}
		}

		public new void update(
			Dictionary<string, object> data,
			int id,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null
		)
		{
			if (IsThereConnection())
			{
				base.update(data, id, res, err, then);
			}
			else
			{
				updateOffline(data, id);
			}
		}

		public new void delete(
			int id,
			ResponseDelegate res = null,
			ErrorDelegate err = null,
			ThenDelegate then = null)
		{
			if (IsThereConnection())
			{
				base.delete(id, res, err, then);
			}
			else
			{
				deleteOffline(id);
			}
		}

	}
}

