﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using Newtonsoft.Json;

namespace RestLibrary.Serializers
{
	public class BaseSerializer : IBaseSerializer
	{
		public string[] Fields { protected set; get; }
		public Type ModelClass { protected set; get; }
		public Dictionary<string, object> Data { protected set; get; }
		protected MultipartFormDataContent _formData;
		public object Model
		{
			get
			{
				return _doDataSerialization();
			}
		}
		public string JSON
		{
			get
			{
				return _doJSONSerialization();
			}
		}
		public MultipartFormDataContent FormData
		{
			get
			{
				return _doFormDataSerialization();
			}
		}
		protected string _doJSONSerialization()
		{
			return JsonConvert.SerializeObject(_doDataSerialization());
		}
		protected MultipartFormDataContent _doFormDataSerialization()
		{
			var model = Activator.CreateInstance(ModelClass);
			var res = new MultipartFormDataContent();
			foreach (KeyValuePair<string, object> d in Data)
			{
				PropertyInfo propertyInfo = model.GetType().GetRuntimeProperty(d.Key);
				if ((propertyInfo != null) && (Array.IndexOf(Fields, d.Key) > -1))
				{
					if (d.Value is string || d.Value is int || d.Value is float)
						res.Add(new StringContent(d.Value.ToString()), d.Key);
					else if (d.Value is Stream)
						res.Add(new StreamContent((Stream)d.Value), d.Key, "image.jpg");
				}
			}
			return res;
		}
		protected object _doDataSerialization()
		{
			var model = Activator.CreateInstance(ModelClass);
			foreach (KeyValuePair<string, object> d in Data)
			{
				PropertyInfo propertyInfo = model.GetType().GetRuntimeProperty(d.Key);
				if (propertyInfo != null)
				{
					if (d.Value is string || d.Value is int || d.Value is float)
						propertyInfo.SetValue(model, d.Value, null);
				}
			}
			return model;
		}
	}
}

