﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniel Aurelio Galeazzo, Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace RestLibrary
{
	public static class Settings
	{
		public const string API_ROOT = "https://cloopsy.unipv.it/rest-api/";
		public const string AUTH_ROOT = "https://cloopsy.unipv.it/rest-auth/";
		public const string HOST = "https://cloopsy.unipv.it";
		public static bool ISONLINE = false;

		public static string LOGINKEY
		{
			get
			{
				return CrossSettings.Current.GetValueOrDefault<string>("LOGINKEY", string.Empty);
			}
			set
			{
				CrossSettings.Current.AddOrUpdateValue<string>("LOGINKEY", value);
			}
		}

		public static string EMAIL
		{
			get { return CrossSettings.Current.GetValueOrDefault<string>("EMAIL", string.Empty);}
			set { CrossSettings.Current.AddOrUpdateValue<string>("EMAIL", value);}
		}

		public static bool TutorialDone
		{
			get
			{
				return CrossSettings.Current.GetValueOrDefault<bool>("TutorialDone", false);
			}
			set
			{
				CrossSettings.Current.AddOrUpdateValue<bool>("TutorialDone", value);
			}
		}

	}
}
