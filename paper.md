---
  title: 'CLOOPSy - Copernicus Land cOver crOwdsourcing Platform for Sentinel-based mapping'
  tags:
    - crowdsourcing
    - land cover
    - mobile app
    - Copernicus
    - Sentinel
    - MYGEOSS
    - participatory sensing
  authors:
   - name: Daniele De Vecchi
     orcid: 0000-0001-8600-7657
     affiliation: 1
   - name: Fabio Dell'Acqua
     orcid: 0000-0002-0044-2998
     affiliation: 1
   - name: Daniel Aurelio Galeazzo
     affiliation: 2 
  affiliations:
   - name: Dep. of Electronic, Computer Science and Biomedical Engineering, University of Pavia, Pavia, Italy
     index: 1
   - name: Ticinum Aerospace
     index: 2
  date: 15 May 2017
  bibliography: paper.bib
  ---

  # Summary

  CLOOPSy is a mobile application designed with the aim to collect data from a crowd of volunteers [@dellacqua_potentials_2017]. In particular, the idea is to ask them to contribute with a picture [@burgelman_rise_2015][@marr_how_????] and to select a land cover class based on the CORINE taxonomy [@laso_bayas_crowdsourcing_2016]. Collected reports, following the administrator approval, will be released to the public and available for download using the APIs. A tutorial is mandatory for each new user. Reports will be used to automatically validate of algorithms working on satellite data, update of land cover layers and to train machine learning algorithms. CLOOPSy is a native mobile app developed using the Xamarin framework [@xamarin]. Reports can only be submitted by registered users. Collected pictures are seasoned with GNSS location and compass direction. Examples for each class of the CORINE Land Cover taxonomy are provided with the aim to help in the decision. The app is built on a general framework, therefore it can be adapted in order to collect different things. Every submitted report is sent to a remote server. It is available for Android on the Google Play store and soon it will be released on the Apple App store. A web portal is also available for CLOOPSy. Volunteers can register and begin to submit reports using the 'Register' page. The main page is a map showing all the public reports and it does not require any login. Reports are considered public only after being approved by super-users. Once a user is logged, the map is filled with all the reports, including those submitted by him/herself and not yet reviewed. The server integrates an algorithm for automatic matching with parcel GIS layers where available. The service is hosted by the ESA RSS (Research and Service Support) service [@esa_rss] for future easy integration with Sentinel-2 data repository. Public and private reports can be download using RESTful (Representational state transfer) APIs. 

  -![CLOOPSy logo.](cloopsy_logo.png)

  # References
