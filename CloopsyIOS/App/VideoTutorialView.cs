/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi, Remote Sensing Group, University of Pavia
*/

using Foundation;
using System;
using UIKit;
using AVFoundation;
using MediaPlayer;

namespace CloopsyIOS
{
    public partial class VideoTutorialView : UIViewController
    {
		AVPlayer _player;
		AVPlayerLayer _playerLayer;
		AVAsset _asset;
		AVPlayerItem _playerItem;
		NSObject videoEndNotificationToken;
		public bool fromInit { get; set;}

        public VideoTutorialView (IntPtr handle) : base (handle)
        {
        }

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			ProceedButton.Enabled = false;

			//NavigationController.SetNavigationBarHidden(false, true);
			_asset = AVAsset.FromUrl(NSUrl.FromFilename("Cloopsy_tutorial_540_960.mp4"));
			_playerItem = new AVPlayerItem(_asset);

			_player = new AVPlayer(_playerItem);
			_playerLayer = AVPlayerLayer.FromPlayer(_player);
			_playerLayer.Frame = View.Frame;
			//VideoView.AddConstraint(NSLayoutConstraint.Create(VideoView,NSLayoutAttribute.Height,NSLayoutRelation.Equal,1,50));
			//_playerLayer.Frame = VideoView.Frame;
			//_playerLayer.Bounds = VideoView.Bounds;
			View.Layer.AddSublayer(_playerLayer);

			_player.Play();

			_player.ActionAtItemEnd = AVPlayerActionAtItemEnd.None;
			videoEndNotificationToken = NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification,VideoDidFinishPlaying,_playerItem);

			ProceedButton.TouchUpInside += delegate {
				AddReportView addReportView = this.Storyboard.InstantiateViewController("AddReportView") as AddReportView;
				if (addReportView != null)
				{
					this.NavigationController.PushViewController(addReportView, true);
				}
			};		
		}

		public override void ViewDidUnload()
		{
			base.ViewDidUnload();
			ReleaseDesignerOutlets();
		}

		//public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
		//{
		//	return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
		//}

		private void VideoDidFinishPlaying(NSNotification obj)
		{
			Console.WriteLine("Video finished");
			if (fromInit == false)
			{
				RestLibrary.Settings.TutorialDone = true;
				//ProceedButton.Enabled = true;
				AddReportView addReportView = this.Storyboard.InstantiateViewController("AddReportView") as AddReportView;
				if (addReportView != null)
				{
					this.NavigationController.PushViewController(addReportView, true);
				}
			}
			else 
			{
				ViewController initialView = this.Storyboard.InstantiateViewController("ViewController") as ViewController;
				if (initialView != null)
				{
					this.NavigationController.PushViewController(initialView, true);
				}
			}
			//RestLibrary.Settings.TutorialDone = true;

		}

    }
}