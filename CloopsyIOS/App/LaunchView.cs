/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/
using Foundation;
using System;
using UIKit;
using RestLibrary.Services;
using SystemConfiguration;

namespace CloopsyIOS
{
    public partial class LaunchView : UIViewController
    {
        public LaunchView (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			bool isLogged = false;
			base.ViewDidLoad();
			NavigationController.SetNavigationBarHidden(true, true);
			// Perform any additional setup after loading the view, typically from a nib.
			//RestLibrary.Settings.ISONLINE = IsHostReachable(RestLibrary.Settings.HOST);
			RestLibrary.Settings.ISONLINE = Services.IsThereConnection();
			Console.WriteLine("Connected:" + RestLibrary.Settings.ISONLINE);

			if (!RestLibrary.Settings.ISONLINE && RestLibrary.Settings.LOGINKEY == string.Empty)
			{
				UIAlertView alertDialog = new UIAlertView()
				{
					Title = "Login failed",
					Message = "You're offline and you have never logged in from this device"
				};
				alertDialog.AddButton("OK");
				alertDialog.Show();
			}
			else if (!RestLibrary.Settings.ISONLINE && RestLibrary.Settings.LOGINKEY != string.Empty)
			{
				UIAlertView alertDialog = new UIAlertView()
				{
					Title = "Offline Login",
					Message = "You're offline but you are logged in"
				};
				alertDialog.AddButton("OK");
				alertDialog.Show();
				isLogged = Services.Bootstrap();
				isLogged = true;
			}
			else
			{
				isLogged = Services.Bootstrap();
				if (isLogged == false)
				{
					ViewController viewController = this.Storyboard.InstantiateViewController("ViewController") as ViewController;
					if (viewController != null)
					{
						this.NavigationController.PushViewController(viewController, true);
					}
				}
			}

			Services.UserService.get(
						 (res) =>
						 {
							 System.Console.WriteLine(res);
							 if (RestLibrary.Settings.TutorialDone)
							 {
								 AddReportView addReportView = this.Storyboard.InstantiateViewController("AddReportView") as AddReportView;
								 if (addReportView != null)
								 {
									 this.NavigationController.PushViewController(addReportView, true);
								 }
							 }
							 else
							 {
								 //TutorialView tutorialView = this.Storyboard.InstantiateViewController("TutorialView") as TutorialView;
								 //if (tutorialView != null)
								 //{
								 // this.NavigationController.PushViewController(tutorialView, true);
								 //}
								 VideoTutorialView videoTutorialView = this.Storyboard.InstantiateViewController("VideoTutorialView") as VideoTutorialView;
								 if (videoTutorialView != null)
								 {
									 videoTutorialView.fromInit = false;
									 this.NavigationController.PushViewController(videoTutorialView, true);
								 }
							 }

						 },
						 (err) =>
						 {
							 ViewController viewController = this.Storyboard.InstantiateViewController("ViewController") as ViewController;
							 if (viewController != null)
							 {
								 this.NavigationController.PushViewController(viewController, true);
							 }
						 }
					 );
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}


    }
}