/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Foundation;
using System;
using UIKit;
using System.Threading.Tasks;
using MobileCoreServices;
using CoreLocation;
using System.Collections.Generic;

using Cloopsy;
using Newtonsoft.Json.Linq;
using System.Linq;
using CoreFoundation;

namespace CloopsyIOS
{
	public partial class CameraView : UIViewController
    {
		static NSString categoryCellId = new NSString("CategoryCell");
		private int _parentId;
		private JToken _parentCategory;
		//public List<String> inputList { get; set;}
		//public List<string> inputList = new List<string>();

        public CameraView (IntPtr handle) : base (handle)
        {
			
			//if (ReportDetail.macrocategory == null)
			//{
			//	inputList = Categories.MacroCategory;
			//}
			//else if (ReportDetail.category == null)
			//{
			//	switch (ReportDetail.macrocategory)
			//	{
			//		case "1":
			//			inputList = Categories.CategoryArtificial;
			//			break;
			//		case "2":
			//			inputList = Categories.CategoryAgricultural;
			//			break;
			//		case "3":
			//			inputList = Categories.CategoryForest;
			//			break;
			//		case "4":
			//			inputList = Categories.CategoryWater;
			//			break;
			//		case "5":
			//			inputList = Categories.CategoryWetlands;
			//			break;
			//		case "6":
			//			inputList = Categories.CategoryWater;
			//			break;
			//	}
			//}
			//else if (ReportDetail.subcategory == null)
			//{
			//	switch (ReportDetail.category)
			//	{
			//		case "1.1":
			//			inputList = Categories.SubCategoryUrban;
			//			break;
			//		case "1.2":
			//			inputList = Categories.SubCategoryIndustrial;
			//			break;
			//		case "1.3":
			//			inputList = Categories.SubCategoryMine;
			//			break;
			//		case "1.4":
			//			inputList = Categories.SubCategoryNonAgricultural;
			//			break;
			//		case "2.1":
			//			inputList = Categories.SubCategoryArable;
			//			break;
			//		case "2.2":
			//			inputList = Categories.SubCategoryPermanent;
			//			break;
			//		case "2.3":
			//			inputList = Categories.SubCategoryHeterogeneous;
			//			break;
			//		case "3.1":
			//			inputList = Categories.SubCategoryForest;
			//			break;
			//		case "3.2":
			//			inputList = Categories.SubCategoryShrub;
			//			break;
			//		case "3.3":
			//			inputList = Categories.SubCategoryOpen;
			//			break;
			//		case "4.1":
			//			inputList = Categories.SubCategoryInland;
			//			break;
			//		case "4.2":
			//			inputList = Categories.SubCategoryCoastal;
			//			break;
			//		case "5.1":
			//			inputList = Categories.SubCategoryInlandWat;
			//			break;
			//		case "5.2":
			//			inputList = Categories.SubCategoryMarine;
			//			break;
			//	}
			//}
			//foreach (string value in inputList)
			//{
			//	mSelectionDataset.Add(new CategoryLayout(Resource.Drawable.cloopsy_logo_final, value));
			//}
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationController.SetNavigationBarHidden(false, true);
			//Console.WriteLine("View load");
			// Perform any additional setup after loading the view, typically from a nib.
			imageView.Image = ReportDetail.bitmap;

			//MacroCategoryTable.RegisterClassForCellReuse(typeof(UITableViewCell), categoryCellId);
			//MacroCategoryTable.Source = new MacroCategoryDataSource(inputList,NavigationController);
			MacroCategoryTable.RegisterClassForCellReuse(typeof(UITableViewCell), categoryCellId);
			MacroCategoryTable.RowHeight = 80;
			SetCategoryAdapter(ReportDetail.parentId);

			//if (ReportDetail.statusCounter == 0)
			//{
			//	ReportDetail.macrocategory = null;
			//	ReportDetail.category = null;
			//	ReportDetail.subcategory = null;
			//}
			//else if (ReportDetail.statusCounter == 1 || (ReportDetail.statusCounter % 4)==0 )
			//{
			//	ReportDetail.category = null;
			//	ReportDetail.subcategory = null;
			//}
			//else if (ReportDetail.statusCounter == 2) || (ReportDetail.statusCounter
			//{
			//	ReportDetail.subcategory = null;
			//}
		}

		public void SetCategoryAdapter(int parentId)
		{
			Services.CategoryService.get(
				parentId,
				(res) =>
				{
					var data = parentId > -1 ? res["children"] : res;
					//https://forums.xamarin.com/discussion/11662/dispatch-queue-t-in-c
					//http://stackoverflow.com/questions/10885268/data-doesnt-load-in-uitableview-until-i-scroll
					DispatchQueue.MainQueue.DispatchAsync(() =>
					{
						MacroCategoryTable.ReloadData();
					});
					_parentId = parentId;
					//ReportDetail.parentId = parentId;
					_parentCategory = res;

					if (data.Count() > 0)
					{
						//var adapter = new CategoryAdapter(this, data);
						//adapter.OnClick += SetCategoryAdapter;
						//categoriesListView.Adapter = adapter;
						MacroCategoryTable.Source = new CategoryDataSource(data, NavigationController);
						
					}
					else
					{
						ReportDetail.category = res["id"].ToString();
						ReportDetail.category_name = res["name"].ToString();
						ReviewAndSubmitView review = this.Storyboard.InstantiateViewController("ReviewAndSubmitView") as ReviewAndSubmitView;
						if (review != null)
						{
							NavigationController.PushViewController(review, true);
						}
					}
				},
				Console.WriteLine,
				Console.WriteLine
			);
		}

		public override void WillMoveToParentViewController(UIViewController parent)
		{
			// http://stackoverflow.com/questions/8228411/detecting-when-the-back-button-is-pressed-on-a-navbar
			base.WillMoveToParentViewController(parent);
			if (parent == null)
			{
				//if (ReportDetail.chooseCategory == 1)
				//{
				//	ReportDetail.macrocategory = null;
				//	ReportDetail.category = null;
				//	ReportDetail.subcategory = null;
				//}
				//else if (ReportDetail.chooseCategory == 2)
				//{
				//	ReportDetail.category = null;
				//	ReportDetail.subcategory = null;
				//	ReportDetail.chooseCategory = 1;
				//}
				//else if (ReportDetail.chooseCategory == 3)
				//{
				//	ReportDetail.subcategory = null;
				//	ReportDetail.chooseCategory = 2;
				//}
				if (_parentId > -1)
				{
					var id = _parentCategory["parent"].Value<string>();
					SetCategoryAdapter(id != null ? int.Parse(id) : -1);
				}
				else
					ReportDetail.ReportReset();
			}
		}

		//class MacroCategoryDataSource : UITableViewSource
		//{
		//	List<String> categories;
		//	UINavigationController controller;
		//	string CellIdentifier = "MacroCategoryCell";

		//	public MacroCategoryDataSource(List<String> elements, UINavigationController controller)
		//	{
		//		this.categories = elements;
		//		this.controller = controller;
		//	}

		//	public override nint RowsInSection(UITableView tableview, nint section)
		//	{
		//		return categories.Count;
		//	}

		//	public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		//	{
		//		var cell = tableView.DequeueReusableCell(CellIdentifier);
		//		string image_code;
		//		string item = categories[indexPath.Row];
		//		if (cell == null)
		//		{
		//			cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier);
		//		}
		//		cell.TextLabel.Text = item;
		//		cell.ImageView.Image = UIImage.FromBundle(item + ".jpg");
		//		return cell;
		//	}

		//	public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		//	{
		//		if (ReportDetail.macrocategory == null)// || ReportDetail.macrocategory!=(indexPath.Row + 1).ToString())
		//		{
		//			//ReportDetail.previousStatus = indexPath.Row;
		//			ReportDetail.macrocategory = (indexPath.Row+1).ToString();
		//			Console.WriteLine(ReportDetail.macrocategory);
		//			CameraView cameraView = controller.Storyboard.InstantiateViewController("CameraView") as CameraView;
		//			if (cameraView != null)
		//			{
		//				ReportDetail.chooseCategory = 1;
		//				controller.PushViewController(cameraView, true);
		//			}
		//		}
		//		else if (ReportDetail.category == null)// || ReportDetail.category != ReportDetail.macrocategory + "." + (indexPath.Row + 1).ToString())
		//		{
		//			//ReportDetail.previousStatus = indexPath.Row;
		//			ReportDetail.category = ReportDetail.macrocategory + "." + (indexPath.Row+1).ToString();
		//			Console.WriteLine(ReportDetail.category);
		//			CameraView cameraView = controller.Storyboard.InstantiateViewController("CameraView") as CameraView;
		//			if (cameraView != null)
		//			{
		//				ReportDetail.chooseCategory = 2;
		//				controller.PushViewController(cameraView, true);
		//			}
		//		}
		//		else if (ReportDetail.subcategory == null)// || ReportDetail.subcategory != ReportDetail.category + "." + (indexPath.Row + 1).ToString())
		//		{
		//			//ReportDetail.previousStatus = indexPath.Row;
		//			ReportDetail.subcategory = ReportDetail.category + "." + (indexPath.Row+1).ToString();
		//			Console.WriteLine(ReportDetail.subcategory);
		//			ReviewAndSubmitView review = controller.Storyboard.InstantiateViewController("ReviewAndSubmitView") as ReviewAndSubmitView;
		//			if (review != null)
		//			{
		//				ReportDetail.chooseCategory = 3;
		//				controller.PushViewController(review, true);
		//			}
		//		}
		//	}
		//}





	}

}