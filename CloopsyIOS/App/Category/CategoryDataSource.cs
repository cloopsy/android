﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using UIKit;
using RestLibrary.Models;
using Newtonsoft.Json.Linq;
using System.Linq;
using Foundation;

namespace CloopsyIOS
{
	//public delegate void OnClickDelegate(int parentId);

	public class CategoryDataSource : UITableViewSource
	{
		JToken categories;
		UINavigationController controller;
		//public OnClickDelegate OnClick;
		string CellIdentifier = "CategoryCell";
		//public event EventHandler<CategorySelectedEventArgs> categorySelected;

		public CategoryDataSource(JToken elements, UINavigationController controller)
		{
			this.categories = elements;
			this.controller = controller;
		}

		public long GetItemId(int position)
		{
			return position;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return categories.Count();
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell(CellIdentifier);
			//string item = categories[indexPath.Row];
			var obj = categories[indexPath.Row];
			var item = (obj != null ? obj["name"].ToString() : "");
			var item_id = (obj != null ? obj["id"].ToString() : "");

			if (cell == null)
			{
				cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier);
			}
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.TextLabel.Text = item.ToString();
			cell.ImageView.Image = UIImage.FromBundle("id" + item_id + ".jpg");
			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			Console.WriteLine("Row Selected: " + categories[indexPath.Row]);
			//if (categorySelected != null)
			//{
			//	categorySelected(this, new CategorySelectedEventArgs(categories[indexPath.Row]["name"].ToString()));
			//}
			ReportDetail.parentId = categories[indexPath.Row]["id"].Value<int>();
			CameraView cameraView = controller.Storyboard.InstantiateViewController("CameraView") as CameraView;
			if (cameraView != null)
			{
				//ReportDetail.chooseCategory = 2;
				controller.PushViewController(cameraView, true);
			}
		}
	}

	//public class CategorySelectedEventArgs : EventArgs
	//{
	//	public string categorySel { get; set; }

	//	public CategorySelectedEventArgs(string categorySel) : base()
	//	{
	//		this.categorySel = categorySel;
	//	}
	//}
}

