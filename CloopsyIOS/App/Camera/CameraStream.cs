/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Foundation;
using System;
using UIKit;
using AVFoundation;
using System.Threading.Tasks;
using CoreGraphics;

namespace CloopsyIOS
{
    public partial class CameraStream : UIViewController
    {
		AVCaptureSession captureSession;
		AVCaptureDevice captureDevice;
		AVCaptureDeviceInput captureDeviceInput;
		AVCaptureStillImageOutput stillImageOutput;
		AVCaptureVideoPreviewLayer videoPreviewLayer;

        public CameraStream (IntPtr handle) : base (handle)
        {
        }

		public override async void ViewDidLoad()
		{
			base.ViewDidLoad();

			await AuthorizeCameraUse();
			NavigationController.SetNavigationBarHidden(true, true);
			SetupLiveCameraStream();
			//UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(TapThat);


			TakePhotoButton.TouchUpInside += async (object sender, EventArgs e) =>
			{
				var videoConnection = stillImageOutput.ConnectionFromMediaType(AVMediaType.Video);
				var sampleBuffer = await stillImageOutput.CaptureStillImageTaskAsync(videoConnection);

				//ReportDetail.Manager.StopLocationUpdates();
				ReportDetail.updateGPSCord = false;
				ReportDetail.updateCompass = false;
				AddReportView.manager.StopLocationUpdates();

				var jpegImageAsNsData = AVCaptureStillImageOutput.JpegStillToNSData(sampleBuffer);
				//var jpegAsByteArray = jpegImageAsNsData.ToArray();

				var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				string jpgFilename = System.IO.Path.Combine(documentsDirectory, "photo.jpg");
				ReportDetail._filename = jpgFilename;

				NSError err = null;
				if (jpegImageAsNsData.Save(jpgFilename, false, out err))
				{
					Console.WriteLine("Saved as " + jpgFilename);
					ReportDetail.bitmap = new UIImage(jpegImageAsNsData);
					CameraView cameraView = this.Storyboard.InstantiateViewController("CameraView") as CameraView;
					if (cameraView != null)
					{
						this.NavigationController.PushViewController(cameraView,true);
					}
				}
				else{
					Console.WriteLine("NOT Saved " + err.LocalizedDescription);
				}
			}; 
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
		}

		void TapThat(UITapGestureRecognizer tap)
		{
			var error = new NSError();
			CGPoint touchPoint = tap.LocationInView(this.liveCameraStream);
			Console.WriteLine("Touch Point: " + touchPoint.X + "," + touchPoint.Y);
			captureDevice.LockForConfiguration(out error);
			if (captureDevice.FocusPointOfInterestSupported)
			{
				captureDevice.FocusPointOfInterest = touchPoint;
				captureDevice.FocusMode = AVCaptureFocusMode.AutoFocus;
				captureDevice.UnlockForConfiguration();
			}
		}

		async Task AuthorizeCameraUse()
		{
			var authorizationStatus = AVCaptureDevice.GetAuthorizationStatus(AVMediaType.Video);

			if (authorizationStatus != AVAuthorizationStatus.Authorized)
			{
				await AVCaptureDevice.RequestAccessForMediaTypeAsync(AVMediaType.Video);
			}
		}

		public void SetupLiveCameraStream()
		{
			captureSession = new AVCaptureSession();

			var viewLayer = liveCameraStream.Layer;
			videoPreviewLayer = new AVCaptureVideoPreviewLayer(captureSession)
			{
				Frame = this.View.Frame
			};
			liveCameraStream.Layer.AddSublayer(videoPreviewLayer);
			this.View.BringSubviewToFront(this.TakePhotoButton);

			UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(TapThat);
			tapGesture.NumberOfTapsRequired = 1;
			tapGesture.NumberOfTouchesRequired = 1;

			liveCameraStream.AddGestureRecognizer(tapGesture);

			captureDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVMediaType.Video);
			ConfigureCameraForDevice(captureDevice);

			captureDeviceInput = AVCaptureDeviceInput.FromDevice(captureDevice);
			captureSession.AddInput(captureDeviceInput);

			var dictionary = new NSMutableDictionary();
			dictionary[AVVideo.CodecKey] = new NSNumber((int)AVVideoCodec.JPEG);
			stillImageOutput = new AVCaptureStillImageOutput()
			{
				OutputSettings = new NSDictionary()
			};

			captureSession.AddOutput(stillImageOutput);
			captureSession.StartRunning();
		}

		void ConfigureCameraForDevice(AVCaptureDevice device)
		{
			var error = new NSError();
			if (device.IsFocusModeSupported(AVCaptureFocusMode.Locked))
			{
				device.LockForConfiguration(out error);
				device.FocusMode = AVCaptureFocusMode.Locked;
				device.UnlockForConfiguration();
			}
			else if (device.IsExposureModeSupported(AVCaptureExposureMode.ContinuousAutoExposure))
			{
				device.LockForConfiguration(out error);
				device.ExposureMode = AVCaptureExposureMode.ContinuousAutoExposure;
				device.UnlockForConfiguration();
			}
			else if (device.IsWhiteBalanceModeSupported(AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance))
			{
				device.LockForConfiguration(out error);
				device.WhiteBalanceMode = AVCaptureWhiteBalanceMode.ContinuousAutoWhiteBalance;
				device.UnlockForConfiguration();
			}
		}
    }
}