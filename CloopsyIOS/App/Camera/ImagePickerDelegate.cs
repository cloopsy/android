﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using Foundation;
using UIKit;

namespace CloopsyIOS
{
	public class ImagePickerDelegate : UIImagePickerControllerDelegate
	{
		//private CameraView vc;

		//public ImagePickerDelegate(CameraView vc)
		//{
		//	this.vc = vc;
		//}
		public ImagePickerDelegate()
		{
			
		}


		public override void FinishedPickingImage(UIImagePickerController picker, UIImage image, Foundation.NSDictionary editingInfo)
		{
			//this.vc.ImageView.Image = image;
			//var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			//string jpgFilename = System.IO.Path.Combine(documentsDirectory, "photo.jpg");
			//NSData imgData = image.AsJPEG();
			//NSError err = null;

			//if (imgData.Save(jpgFilename, false, out err))
			//{
			//	Console.WriteLine("Saved as " + jpgFilename);
			//}
			//else
			//{
			//	Console.WriteLine("Not saved as " + jpgFilename + " because" + err.LocalizedDescription);
			//}
			//ReportDetail.Manager.StopLocationUpdates();
			//ReportDetail.imageTaken = image;
			//picker.DismissViewController(true, null);
		}
	}
}

