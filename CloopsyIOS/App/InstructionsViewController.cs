using Foundation;
using System;
using UIKit;

namespace CloopsyIOS
{
    public partial class InstructionsViewController : UIViewController
    {
        public InstructionsViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			InstrWathTutorialButton.TouchUpInside += (object sender, EventArgs e) =>
			{
				VideoTutorialView videoTutorialView = this.Storyboard.InstantiateViewController("VideoTutorialView") as VideoTutorialView;
				if (videoTutorialView != null)
				{
					videoTutorialView.fromInit = true;
					this.NavigationController.PushViewController(videoTutorialView, true);
				}
			};
		}
    }
}