// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace CloopsyIOS
{
    [Register ("VideoTutorialView")]
    partial class VideoTutorialView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ProceedButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView VideoView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ProceedButton != null) {
                ProceedButton.Dispose ();
                ProceedButton = null;
            }

            if (VideoView != null) {
                VideoView.Dispose ();
                VideoView = null;
            }
        }
    }
}