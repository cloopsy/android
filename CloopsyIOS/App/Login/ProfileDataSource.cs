﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using UIKit;
using RestLibrary.Models;
using Newtonsoft.Json.Linq;
using System.Linq;
using Foundation;
using CoreFoundation;

namespace CloopsyIOS
{

	public class ProfileDataSource : UITableViewSource
	{
		User profileData;
		UINavigationController controller;
		static NSString cellIdentifier = new NSString("ProfileCell");
		//public event EventHandler<CategorySelectedEventArgs> categorySelected;

		public ProfileDataSource(User elements, UINavigationController controller)
		{
			this.profileData = elements;
			//var reverse = elements.Reverse<JToken>();
			//this.reports = (JToken)elements.Reverse();
			this.controller = controller;
		}

		public long GetItemId(int position)
		{
			return position;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return User.FIELDS.Length;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			//var cell = tableView.DequeueReusableCell(cellIdentifier) as UITableViewCell;
			var cell = tableView.DequeueReusableCell(cellIdentifier) as ProfileCell;
			//string item = categories[indexPath.Row];
			var prop = GetItemPropriety(indexPath.Row);
			var obj = profileData.GetType().GetProperty(GetItemPropriety(indexPath.Row)).GetValue(profileData);
			var item_prop = prop.ToString();
			Console.WriteLine(item_prop);
			var item = (obj != null ? obj.ToString() : "");

			if (cell == null)
			{
				cell = new ProfileCell(cellIdentifier);
				//cell = new UITableViewCell(UITableViewCellStyle.Default, cellIdentifier);
			}

			//cell.UpdateCell(item_prop.ToString());
			//cell.TextLabel.Text = item_prop;
			//cell.DetailTextLabel.Text = item;
			//cell.ImageView.Image = UIImage.FromBundle(item_id + ".jpg");
			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			Console.WriteLine("Row Selected: " + profileData.GetType().GetProperty(GetItemPropriety(indexPath.Row)).GetValue(profileData));
			Console.WriteLine("Index Path row: " + indexPath.Row);
		}


		public static string GetItemPropriety(int position)
		{
			return User.FIELDS[position];
		}

	}




	//public class CategorySelectedEventArgs : EventArgs
	//{
	//	public string categorySel { get; set; }

	//	public CategorySelectedEventArgs(string categorySel) : base()
	//	{
	//		this.categorySel = categorySel;
	//	}
	//}
}

