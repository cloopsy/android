/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace CloopsyIOS
{
	public partial class ProfileCell : UITableViewCell//, IUITextFieldDelegate
    {
		//public UITextField labelItem;

		public ProfileCell(IntPtr handle) : base(handle) { }


		public ProfileCell (NSString cellId)
        {
			//Console.WriteLine(cellId);
			////labelItem = new UITextField();
			//ContentView.AddSubview(new UIView { labelItem});
			//SelectionStyle = UITableViewCellSelectionStyle.None;
			//var frame = new CGRect(10, 10, 300, 30);
			//labelItem = new UITextField(frame);
			//labelItem.Placeholder = "Ciao";
			//ContentView.Add(labelItem);
        }

		public void UpdateCell(string caption)
		{
			Console.WriteLine(caption);
			//labelItem.Placeholder = caption;
		}


    }
}