﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using Foundation;
using System.Collections.Generic;
using CoreGraphics;
using UIKit;
using SafariServices;

namespace CloopsyIOS
{
	public partial class ViewController : UIViewController
	{
		
		protected ViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationController.SetNavigationBarHidden(false, true);
			// Perform any additional setup after loading the view, typically from a nib.

			SignInButton.TouchUpInside += (object sender, EventArgs e) =>
			{
				SignInView signInView = this.Storyboard.InstantiateViewController("SignInView") as SignInView;
				if (signInView != null)
				{
					this.NavigationController.PushViewController(signInView, true);
				}
			};

			SignUpButton.TouchUpInside += (object sender, EventArgs e) =>
			{
				int SystemVersion = Convert.ToInt16(UIDevice.CurrentDevice.SystemVersion.Split('.')[0]);
				if (SystemVersion >= 9)
				{
					var url = new NSUrl("https://cloopsy.unipv.it/accounts/signup/");
					var sfViewController = new SFSafariViewController(url);
					PresentViewController(sfViewController,true,null);
				}
				else
				{
					SignUpView signUpView = this.Storyboard.InstantiateViewController("SignUpView") as SignUpView;
					if (signUpView != null)
					{
						this.NavigationController.PushViewController(signUpView, true);
					}
				}
				//UIApplication.SharedApplication.OpenUrl(new NSUrl("https://cloopsy.unipv.it/accounts/signup/"));
			};

			InitCreditsButton.TouchUpInside += (object sender, EventArgs e) =>
			{
				CreditsController creditsController = this.Storyboard.InstantiateViewController("CreditsController") as CreditsController;
				if (creditsController != null)
				{
					this.NavigationController.PushViewController(creditsController, true);
				}
			};

			InstructionsButton.TouchUpInside += (object sender, EventArgs e) =>
			{
				InstructionsViewController instructionsView = this.Storyboard.InstantiateViewController("InstructionsViewController") as InstructionsViewController;
				if (instructionsView != null)
				{
					this.NavigationController.PushViewController(instructionsView, true);
				}
			};

		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}


	}


}

