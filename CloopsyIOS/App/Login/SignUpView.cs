using Foundation;
using System;
using UIKit;
using WebKit;

namespace CloopsyIOS
{
    public partial class SignUpView : UIViewController
    {
		
        public SignUpView (IntPtr handle) : base (handle)
        {
        }

		//https://forums.xamarin.com/discussion/13458/how-to-open-link-in-ios-safari-browser
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			//WebSignUp.ShouldStartLoad = HandleShouldStartLoad;
			//WebSignUp.LoadRequest(new NSUrlRequest(new NSUrl("https://cloopsy.unipv.it/accounts/signup/")));
			WKWebView webView = new WKWebView(View.Frame, new WKWebViewConfiguration());
			View.AddSubview(webView);
			var url = new NSUrl("https://cloopsy.unipv.it/accounts/signup/");
			var request = new NSUrlRequest(url);
			webView.LoadRequest(request);
		}

		//bool HandleShouldStartLoad(UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
		//{
		//	// Filter out clicked links
		//	if (navigationType == UIWebViewNavigationType.LinkClicked)
		//	{
		//		if (UIApplication.SharedApplication.CanOpenUrl(request.Url))
		//		{
		//			// Open in Safari instead
		//			UIApplication.SharedApplication.OpenUrl(request.Url);
		//			return false;
		//		}
		//	}

		//	return true;
		//}
    }
}