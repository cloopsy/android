/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Foundation;
using System;
using UIKit;
using CoreFoundation;
using RestLibrary.Models;
using System.Collections.Generic;

namespace CloopsyIOS
{
    public partial class ProfileController : UIViewController
    {
		static NSString profileCellId = new NSString("ProfileCell");

        public ProfileController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			//ProfileTable.RegisterClassForCellReuse(typeof(ProfileCell), profileCellId);
			//ProfileTable.RegisterClassForCellReuse(typeof(ProfileCell), profileCellId);
			EmailText.Enabled = false;


			FirstNameText.ShouldReturn += CloseKeyboard;
			LastNameText.ShouldReturn += CloseKeyboard;
			EmailText.ShouldReturn += CloseKeyboard;
			AddressText.ShouldReturn += CloseKeyboard;
			//AgeText.ShouldReturn += CloseKeyboard;
			TelephoneText.ShouldReturn += CloseKeyboard;
			OrganizationText.ShouldReturn += CloseKeyboard;

			SetProfileListAdapter();
			if (RestLibrary.Settings.ISONLINE)
			{
				LogoutButton.TouchUpInside += delegate
				{
					Services.AuthService.logout(then: () =>
					{
						ViewController loginController = this.Storyboard.InstantiateViewController("ViewController") as ViewController;
						if (loginController != null)
						{
							this.NavigationController.PushViewController(loginController, true);
						}
					});
				};

				UpdateButton.TouchUpInside += delegate {
					//var data = UserEditAdapter.getDataFromView(userListView);
					//"email", "address", "telephone", "age", "organization"
					var output = new Dictionary<string, object>();
					output.Add("first_name", FirstNameText.Text);
					output.Add("last_name", LastNameText.Text);
					output.Add("email", EmailText.Text);
					output.Add("telephone", TelephoneText.Text);
					//output.Add("age", AgeText.Text);
					output.Add("organization", OrganizationText.Text);
					Services.UserService.update(
						output,
						(res) => { Console.WriteLine(res); },
						(err) => { Console.WriteLine(err); }
					);
				};
			}	
		}

		public void SetProfileListAdapter()
		{
			if (RestLibrary.Settings.ISONLINE)
			{
				Services.UserService.get(
				(res) =>
				{
					//DispatchQueue.MainQueue.DispatchAsync(() =>
					//{
					//	ProfileTable.ReloadData();
					//});
					User user = res.ToObject<User>();
					Console.WriteLine("Manage profile");
					Console.WriteLine(user);
					//userListView.Adapter = new UserEditAdapter(this, user);
					//ProfileTable.Source = new ProfileDataSource(user, NavigationController);
					FirstNameText.Text = user.first_name;
					LastNameText.Text = user.last_name;
					EmailText.Text = user.email;
					AddressText.Text = user.address;
					//AgeText.Text = user.age;
					TelephoneText.Text = user.telephone;
					OrganizationText.Text = user.organization;
				},
				(error) => { Console.WriteLine(error); }
				);
			}
		}

		public bool CloseKeyboard(UITextField txtField)
		{
			txtField.ResignFirstResponder();
			return true;
		}
    }
}