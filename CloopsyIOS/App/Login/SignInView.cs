/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Foundation;
using System;
using UIKit;
using System.Collections.Generic;

namespace CloopsyIOS
{
    public partial class SignInView : UIViewController
    {
        public SignInView (IntPtr handle) : base (handle)
        {
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			EmailText.KeyboardType = UIKeyboardType.EmailAddress;
			PasswordText.KeyboardType = UIKeyboardType.Default;
			PasswordText.SecureTextEntry = true;

			LoginButton.TouchUpInside += (object sender, EventArgs e) =>
			{
				Console.WriteLine(EmailText.Text);
				Console.WriteLine(PasswordText.Text);

				var authService = new RestLibrary.Services.AuthManager();
				authService.login(
					new Dictionary<string, object>()
					{
					{"email", EmailText.Text},
					{"password", PasswordText.Text}
					},
					(res) =>
					{
						Console.WriteLine(res);
						VideoTutorialView tutorialView = this.Storyboard.InstantiateViewController("VideoTutorialView") as VideoTutorialView;
						if (tutorialView != null)
						{
							tutorialView.fromInit = false;
							this.NavigationController.PushViewController(tutorialView, true);
						}
					},
					(err) =>
					{
						Console.WriteLine(err); //var alertDialog = builder.Create();
						UIAlertView alertDialog = new UIAlertView()
						{
							Title = "Login Failed",
							Message = err
						};
						alertDialog.AddButton("OK");
						alertDialog.Show();
					});

			};

		}
    }
}