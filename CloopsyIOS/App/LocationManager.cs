﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using CoreLocation;
using UIKit;

namespace CloopsyIOS
{
	public class LocationManager
	{
		protected CLLocationManager locationManager;

		public LocationManager()
		{
			this.locationManager = new CLLocationManager();
			this.locationManager.PausesLocationUpdatesAutomatically = false;

			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
				locationManager.RequestWhenInUseAuthorization();
			}

			if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
			{
				locationManager.AllowsBackgroundLocationUpdates = true;
			}
		}

		public CLLocationManager LocMgr
		{
			get { return this.locationManager; }
		}

		public void StartLocationUpdates()
		{
			if (CLLocationManager.LocationServicesEnabled)
			{
				locationManager.DesiredAccuracy = CLLocation.AccuracyBest;
				locationManager.HeadingFilter = 1;
				locationManager.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
				{
					LocationUpdated(this, new LocationUpdateEventArgs(e.Locations[e.Locations.Length - 1]));
				};
				locationManager.UpdatedHeading += (object sender, CLHeadingUpdatedEventArgs e) =>
				{
					HeadingUpdated(this, new HeadingUpdateEventArgs(e.NewHeading));
				};
				locationManager.StartUpdatingLocation();
				locationManager.StartUpdatingHeading();

			}	
		}

		public void StopLocationUpdates()
		{
			if (CLLocationManager.LocationServicesEnabled)
			{
				locationManager.StopUpdatingLocation();
				locationManager.StopUpdatingHeading();
			}
		}

		public event EventHandler<LocationUpdateEventArgs> LocationUpdated = delegate { };
		public event EventHandler<HeadingUpdateEventArgs> HeadingUpdated = delegate { };
	}
}

