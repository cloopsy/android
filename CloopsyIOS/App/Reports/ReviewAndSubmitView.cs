/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using System.IO;

namespace CloopsyIOS
{
    public partial class ReviewAndSubmitView : UIViewController
    {
		static NSString macroCategoryCellId = new NSString("MacroCategoryCell");
		public List<string> inputList;

        public ReviewAndSubmitView (IntPtr handle) : base (handle)
        {
			inputList = new List<string> { ""+ReportDetail.latitude, ""+ReportDetail.longitude, ""+ReportDetail.compass,""+ReportDetail.category,""+ReportDetail.category_name};
        }

		public override void ViewDidLoad()
		{
			imageView.Image = ReportDetail.bitmap;
			ReviewTable.RegisterClassForCellReuse(typeof(UITableViewCell), macroCategoryCellId);
			ReviewTable.Source = new ReviewDataSource(inputList, NavigationController);

			var stream = new FileStream(ReportDetail._filename, FileMode.Open);
			ReportDetail.telephone_id = UIDevice.CurrentDevice.IdentifierForVendor.AsString();
			Console.WriteLine("Phone ID: " + ReportDetail.telephone_id);

			SubmitButton.TouchUpInside += (object sender, EventArgs e) =>
			{
				Services.ReportService.create(
					new Dictionary<string, object>()
					{
						{"category", ReportDetail.category},
						{"position", ReportDetail.longitude + ", " + ReportDetail.latitude},
						{"direction", ReportDetail.compass},
						{"distance_focus", ReportDetail.distance_focus ?? "DISTANCE FOCUS NO AVAILABLE"},
						{"telephone_id", ReportDetail.telephone_id},
						//{"compass", ReportDetail.compass ?? "NO AVAILABLE COMPASS"},
						{"image", stream}
					},
					(res) =>
					{
						AddReportView addReportView = this.Storyboard.InstantiateViewController("AddReportView") as AddReportView;
						addReportView.SubmitReportResult = res.ToString();
						Console.WriteLine(res);
						stream.Close();
						ReportDetail.ReportReset();
						UIAlertView alertDialog = new UIAlertView()
						{
							Title = "Report Submitted",
							Message = res.ToString()
						};
						alertDialog.AddButton("OK");
						alertDialog.Show();
						if (addReportView != null)
						{
							this.NavigationController.PushViewController(addReportView, true);
						}
					},
					(err) =>
					{
						AddReportView addReportView = this.Storyboard.InstantiateViewController("AddReportView") as AddReportView;
						stream.Close();
						Console.WriteLine(err);
						addReportView.SubmitReportResult = err;
						ReportDetail.ReportReset();
						UIAlertView alertDialog = new UIAlertView()
						{
							Title = "Submission failed",
							Message = err
						};
						alertDialog.AddButton("OK");
						alertDialog.Show();
						if (addReportView != null)
						{
							this.NavigationController.PushViewController(addReportView, true);
						}
					}
				);

			};
		}
    }

	class ReviewDataSource : UITableViewSource
	{
		List<String> categories;
		UINavigationController controller;
		string CellIdentifier = "MacroCategoryCell";

		public ReviewDataSource(List<String> elements, UINavigationController controller)
		{
			this.categories = elements;
			this.controller = controller;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return categories.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell(CellIdentifier);
			string item = categories[indexPath.Row];
			if (cell == null)
			{
				cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier);
			}
			cell.TextLabel.Text = item;
			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			
		}
	}
}