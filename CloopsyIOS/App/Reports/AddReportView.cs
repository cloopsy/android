/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Foundation;
using System;
using UIKit;
using SidebarNavigation;
using CoreLocation;
using System.Collections.Generic;
using CoreGraphics;
using CoreFoundation;
using SystemConfiguration;

namespace CloopsyIOS
{
	//public static class ReportDetail
	//{
	//	public static UIImage imageTaken;
	//	public static CLLocation location;
	//	public static string compass;
	//	public static string macrocategory = null;
	//	public static string category = null;
	//	public static string subcategory = null;
	//	public static int chooseCategory = 1;
	//	public static LocationManager Manager { get; set; }

	//}

    public partial class AddReportView : UIViewController
    {
		//public SidebarController SidebarController { get; set;}
		public string SubmitReportResult { get; set; }
		public static LocationManager manager { get; set; }
		static NSString reportCellId = new NSString("ReportCell");
		TableDelegate tableDelegate;

		#region Computed Properties
		public static bool UserInterfaceIdiomIsPhone
		{
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}


		#endregion

		public AddReportView (IntPtr handle) : base (handle)
        {
			//ReportDetail.Manager = new LocationManager();
			//ReportDetail.Manager.StartLocationUpdates();
			//ReportDetail.Manager.LocationUpdated += HandleLocationChanged;
			//ReportDetail.Manager.HeadingUpdated += HandleHeadingChanged;
			manager = new LocationManager();
			manager.StartLocationUpdates();
			manager.LocationUpdated += HandleLocationChanged;
			manager.HeadingUpdated += HandleHeadingChanged;
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationController.SetNavigationBarHidden(false, true);
			// Perform any additional setup after loading the view, typically from a nib.
			//this.NavigationController.NavigationBar.TintColor = UIColor.White;
			//this.NavigationController.NavigationBar.BarTintColor = UIColor.Cyan;

			//SidebarController = new SidebarController(this, NavigationController, new SideMenuController(SidebarController));
			//SidebarController.HasShadowing = false;
			//SidebarController.MenuWidth = 220;
			//SidebarController.MenuLocation = SidebarController.MenuLocations.Left;

			this.NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Edit, (sender, e) => {

				MenuController menuView = this.Storyboard.InstantiateViewController("MenuController") as MenuController;
				if (menuView != null)
				{
					this.NavigationController.PushViewController(menuView, true);
				}
			}),true);

			//http://douglasstarnes.com/index.php/2016/03/13/simple-camera-access-with-ios-xamarin/
			AddReportButton.TouchUpInside += (object sender, EventArgs e) =>
			{
				//var imagePicker = new UIImagePickerController();
				//imagePicker.SourceType = UIImagePickerControllerSourceType.Camera;
				//imagePicker.Editing = false;
				//imagePicker.CameraCaptureMode = UIImagePickerControllerCameraCaptureMode.Photo;
				//imagePicker.CameraDevice = UIImagePickerControllerCameraDevice.Rear;

				//var imagePickerDelegate = new ImagePickerDelegate();
				//imagePicker.Delegate = imagePickerDelegate;

				//NavigationController.PresentModalViewController(imagePicker, true);
				//CameraView cameraView = this.Storyboard.InstantiateViewController("CameraView") as CameraView;
				//if (cameraView != null)
				//{
				//	this.NavigationController.PushViewController(cameraView, true);
				//}
				if (!RestLibrary.Settings.ISONLINE)
				{
					Console.WriteLine("No connection available");
					ReportDetail.offline = true;
				}
				else 
				{
					Console.WriteLine("Connection available");
					ReportDetail.offline = false;
				}

				CameraStream cameraStream = this.Storyboard.InstantiateViewController("CameraStream") as CameraStream;
				if (cameraStream != null)
				{
					this.NavigationController.PushViewController(cameraStream, true);
				}
			};

			ReportListTable.RegisterClassForCellReuse(typeof(UITableViewCell),reportCellId);
			ReportListTable.RowHeight = 60;

			SetReportListAdapter();

		}

		public void HandleLocationChanged(object sender, LocationUpdateEventArgs e)
		{
			CLLocation location = e.Location;

			//Console.WriteLine("Latitude: " + location.Coordinate.Latitude.ToString());
			//Console.WriteLine("Longitude: " + location.Coordinate.Longitude.ToString());
			//ReportDetail.location = location;
			ReportDetail.latitude = location.Coordinate.Latitude.ToString();
			ReportDetail.longitude = location.Coordinate.Longitude.ToString();
		}

		public void HandleHeadingChanged(object sender, HeadingUpdateEventArgs e)
		{
			CLHeading heading = e.Heading;
			ReportDetail.compass = heading.TrueHeading.ToString();
			//Console.WriteLine("Heading: " + heading.TrueHeading.ToString());
		}

		public void SetReportListAdapter()
		{
			if (RestLibrary.Settings.ISONLINE)
			{
				Services.ReportService.list(
				(res) =>
				{
					DispatchQueue.MainQueue.DispatchAsync(() =>
					{
						ReportListTable.ReloadData();
					});
					//var adapter = new ReportListAdapter(ListView.Context, res);
					//adapter.OnDeleted += SetReportListAdapter;
					//this.ListAdapter = adapter;
					ReportListTable.Source = new ReportDataSource(res, NavigationController);
					tableDelegate = new TableDelegate(res, NavigationController);
					ReportListTable.Delegate = tableDelegate;
				},
				System.Console.WriteLine,
				System.Console.WriteLine
				);
			}

		}

    }


}