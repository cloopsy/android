﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using UIKit;
using RestLibrary.Models;
using Newtonsoft.Json.Linq;
using System.Linq;
using Foundation;
using CoreFoundation;

namespace CloopsyIOS
{

	public class ReportDataSource : UITableViewSource
	{
		JToken reports;
		UINavigationController controller;
		string CellIdentifier = "ReportCell";
		//public event EventHandler<CategorySelectedEventArgs> categorySelected;

		public ReportDataSource(JToken elements, UINavigationController controller)
		{
			this.reports = elements;
			//var reverse = elements.Reverse<JToken>();
			//this.reports = (JToken)elements.Reverse();
			this.controller = controller;
		}

		public long GetItemId(int position)
		{
			return position;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return reports.Count();
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell(CellIdentifier);
			//string item = categories[indexPath.Row];
			var obj = reports[indexPath.Row];
			var item = (obj != null ? obj["created_at"].ToString() : "");

			if (cell == null)
			{
				cell = new UITableViewCell(UITableViewCellStyle.Default, CellIdentifier);
			}

			cell.TextLabel.Text = item.ToString();
			//cell.ImageView.Image = UIImage.FromBundle(item_id + ".jpg");
			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			Console.WriteLine("Row Selected: " + reports[indexPath.Row]);
			Console.WriteLine("Index Path row: " + indexPath.Row);
		}

		//public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
		//{
		//	//base.CommitEditingStyle(tableView, editingStyle, indexPath);
		//	switch (editingStyle)
		//	{
		//		case UITableViewCellEditingStyle.Delete:
		//			reports[indexPath.Row].Remove();
		//			Services.ReportService.delete(
		//				(int)reports[indexPath.Row]["id"]//,
		//			//	delegate
		//			//	{
		//			//		OnDeleted();
		//			//	},
		//			//	delegate
		//			//	{
		//			//		OnDeleted();
		//			//	}
		//			);
		//			tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);

		//			//Services.ReportService.list(
		//			//	(res) =>
		//			//	{
		//			//		DispatchQueue.MainQueue.DispatchAsync(() =>
		//			//		{
		//			//			tableView.ReloadData();
		//			//		});
		//			//		//var adapter = new ReportListAdapter(ListView.Context, res);
		//			//		//adapter.OnDeleted += SetReportListAdapter;
		//			//		//this.ListAdapter = adapter;
		//			//		this.reports = res;
		//			//	},
		//			//	System.Console.WriteLine,
		//			//	System.Console.WriteLine
		//			//);
		//			break;
					
		//		case UITableViewCellEditingStyle.None:
		//			Console.WriteLine("CommitEditingStyle:None called");
		//			break;
		//	}
		//}

		public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
		{
			return true;
		}

	}

	public class TableDelegate : UITableViewDelegate
	{
		JToken reports;
		UINavigationController controller;

		public TableDelegate(JToken elements, UINavigationController controller) 
		{
			this.reports = elements;
			this.controller = controller;
		}

		public TableDelegate(IntPtr handle) : base(handle){ }
		public TableDelegate(NSObjectFlag t) : base(t) { }

		public override UITableViewRowAction[] EditActionsForRow(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewRowAction editButton = UITableViewRowAction.Create(
				UITableViewRowActionStyle.Default,
				" Edit ",
				delegate {
					Console.WriteLine("Edit record");
					Console.WriteLine(reports[indexPath.Row]);
					Console.WriteLine(indexPath.Row);
			});
			editButton.BackgroundColor = UIColor.Blue;

			UITableViewRowAction deleteButton = UITableViewRowAction.Create(
				UITableViewRowActionStyle.Destructive,
				"Delete",
				delegate {
					Console.WriteLine("Delete record");
					Services.ReportService.delete((int)reports[indexPath.Row]["id"]);
					Console.WriteLine((int)reports[indexPath.Row]["id"]);
					reports[indexPath.Row].Remove();
					tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
			});
			deleteButton.BackgroundColor = UIColor.Red;
			//return base.EditActionsForRow(tableView, indexPath);
			return new UITableViewRowAction[] { deleteButton,editButton };
		}

	}

	//public class CategorySelectedEventArgs : EventArgs
	//{
	//	public string categorySel { get; set; }

	//	public CategorySelectedEventArgs(string categorySel) : base()
	//	{
	//		this.categorySel = categorySel;
	//	}
	//}
}

