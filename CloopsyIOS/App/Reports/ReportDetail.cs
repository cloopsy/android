﻿/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Author: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using System;
using System.IO;
using UIKit;

namespace CloopsyIOS
{
	public delegate void ReportDone();
	public delegate void ValidGPS();
	public delegate void InvalidGPS();
	public static class ReportDetail
	{
		public static FileStream _file;
		public static string _filename;
		//public static File _dir;
		public static UIImage bitmap;
		private static string _latitude;
		public static string latitude
		{
			get
			{
				return _latitude;
			}
			set
			{
				if (updateGPSCord)
					_latitude = value;
			}
		}
		private static string _longitude;
		public static string longitude
		{
			get
			{
				return _longitude;
			}
			set
			{
				if (updateGPSCord)
					_longitude = value;
			}
		}
		public static bool updateGPSCord = true;
		public static ValidGPS validGPS;
		public static InvalidGPS invalidGPS;
		//private static Availability _statusGPS;
		//public static Availability StatusGPS
		//{
		//	get
		//	{
		//		return _statusGPS;
		//	}
		//	set
		//	{
		//		_statusGPS = value;
		//		if (_statusGPS == Availability.Available && validGPS != null)
		//			validGPS();
		//		else if ((_statusGPS == Availability.OutOfService || _statusGPS == Availability.TemporarilyUnavailable)
		//				 && invalidGPS != null)
		//			invalidGPS();
		//	}
		//}
		private static string _compass;
		public static string compass
		{
			get
			{
				return _compass;
			}
			set
			{
				if (updateCompass)
					_compass = value;
			}
		}
		//public static string compass;
		public static bool updateCompass = true;
		public static int chooseCategory;
		public static string macrocategory;
		public static string category;
		public static string category_name;
		public static string subcategory;
		public static string distance_focus;
		public static string telephone_id;
		public static int parentId = -1;
		public static bool offline;
		public static void ReportReset()
		{
			_file = null;
			//_dir = null;
			if (bitmap != null)
				bitmap.Dispose();
			bitmap = null;
			_latitude = null;
			longitude = null;
			compass = null;
			macrocategory = null;
			subcategory = null;
			category = null;
			distance_focus = null;
			telephone_id = null;
			updateGPSCord = true;
			parentId = -1;
		}
		private static System.Timers.Timer _fakeGPS;
		//public static void FakeGPS()
		//{
		//	if (_fakeGPS == null)
		//	{
		//		StatusGPS = Availability.OutOfService;
		//		_fakeGPS = new System.Timers.Timer(2000);
		//		_fakeGPS.Elapsed +=
		//			delegate
		//			{
		//				StatusGPS = (StatusGPS == Availability.Available ? Availability.OutOfService : Availability.Available);
		//				latitude = "FAKE LATITUDE";
		//				longitude = "FAKE LONGITUDE";
		//			};
		//		_fakeGPS.AutoReset = true;
		//		_fakeGPS.Enabled = true;
		//	}
		//}
	}
}

