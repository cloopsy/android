// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace CloopsyIOS
{
    [Register ("MenuController")]
    partial class MenuController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CreditsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton OfflineReportsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ProfileButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton WatchTutorialButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CreditsButton != null) {
                CreditsButton.Dispose ();
                CreditsButton = null;
            }

            if (OfflineReportsButton != null) {
                OfflineReportsButton.Dispose ();
                OfflineReportsButton = null;
            }

            if (ProfileButton != null) {
                ProfileButton.Dispose ();
                ProfileButton = null;
            }

            if (WatchTutorialButton != null) {
                WatchTutorialButton.Dispose ();
                WatchTutorialButton = null;
            }
        }
    }
}