/*
* Copyright 2016 Joint Research Centre
*
* Licensed under the EUPL, Version 1.1 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
* 
* Authors: Daniele De Vecchi (Remote Sensing Group, University of Pavia)
*/

using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;

namespace CloopsyIOS
{
    public partial class TutorialView : UIViewController
    {
		private UIPageViewController pageViewController;
		private List<string> pageTitles;

        public TutorialView (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationController.SetNavigationBarHidden(false, true);
			pageTitles = new List<string> { "Page 1", "Page 2", "Page 3" };

			pageViewController = this.Storyboard.InstantiateViewController("TutorialPageViewController") as UIPageViewController;
			pageViewController.DataSource = new PageViewControllerDataSource(this, pageTitles);

			var startVC = this.ViewControllerAtIndex(0) as ContentViewController;
			var viewControllers = new UIViewController[] { startVC };

			pageViewController.SetViewControllers(viewControllers, UIPageViewControllerNavigationDirection.Forward, false, null);
			pageViewController.View.Frame = new CGRect(0, 0, this.View.Frame.Width, this.View.Frame.Size.Height - 50);
			AddChildViewController(this.pageViewController);
			View.AddSubview(this.pageViewController.View);
			pageViewController.DidMoveToParentViewController(this);

			SkipButton.TouchUpInside += SkipTutorial;
		}

		private void SkipTutorial(object sender, EventArgs e)
		{
			var vc = this.Storyboard.InstantiateViewController("AddReportView") as AddReportView;
			if (vc != null)
			{
				this.NavigationController.PushViewController(vc, true);
			}
		}

		public UIViewController ViewControllerAtIndex(int index)
		{
			var vc = this.Storyboard.InstantiateViewController("ContentViewController") as ContentViewController;
			vc.titleText = pageTitles.ElementAt(index);
			//vc.imageFile = _images.ElementAt(index);
			vc.pageIndex = index;
			return vc;
		}

		private class PageViewControllerDataSource : UIPageViewControllerDataSource
		{
			private TutorialView _parentViewController;
			private List<string> _pageTitles;

			public PageViewControllerDataSource(UIViewController parentViewController, List<string> pageTitles)
			{
				_parentViewController = parentViewController as TutorialView;
				_pageTitles = pageTitles;
			}

			public override UIViewController GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
			{
				var vc = referenceViewController as ContentViewController;
				var index = vc.pageIndex;
				if (index == 0)
				{
					return null;
				}
				else {
					index--;
					return _parentViewController.ViewControllerAtIndex(index);
				}
			}

			public override UIViewController GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
			{
				var vc = referenceViewController as ContentViewController;
				var index = vc.pageIndex;

				index++;
				if (index == _pageTitles.Count)
				{
					return null;
				}
				else {
					return _parentViewController.ViewControllerAtIndex(index);
				}
			}

			public override nint GetPresentationCount(UIPageViewController pageViewController)
			{
				return _pageTitles.Count;
			}

			public override nint GetPresentationIndex(UIPageViewController pageViewController)
			{
				return 0;
			}
		}
    }
}