# README #

Cloopsy is a mobile application designed with the aim to collect data from a crowd of volunteers. In particular, the idea is to ask them to contribute with a picture and to select a land cover class based on the CORINE taxonomy.

### Acknowledgement ###
This application has been developed within the MYGEOSS project, which has received funding from the European Union's Horizon 2020 research and innovation programme. The JRC, or as the case may be the European Commission, shall not be held liable for any direct or indirect, incidental, consequential or other damages, including but not limited to the loss of data, loss of profits, or any other financial loss arising from the use of this application, or inability to use it, even if the JRC is notified of the possibility of such damages.

### What is this repository for? ###

* Quick summary
This repository contains source of the native Android and iOS mobile apps (written using Xamarin). The apps are released in thei respective stores. 
* Version
iOS 1.04, Android 1.01
* Tutorial: https://www.youtube.com/watch?v=BFrwMdytJI0
* Website: https://cloopsy.unipv.it/

### How do I get set up? ###

* Summary of set up
Download the apps from the respective stores. The other solution is to compile the code using Xamarin Studio (XCode is also necessary).

### Who do I talk to? ###

* Repo owner: MyGEOSS (Joint Research Centre) and Daniele De Vecchi (Remote Sensing Group, UniversitÃ  degli Studi di Pavia)
* Code is released under the EUPL License